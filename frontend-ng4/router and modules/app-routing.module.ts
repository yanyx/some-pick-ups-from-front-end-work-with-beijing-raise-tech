/**
 * Created by Yunxuan Yan on 2016/12/16.
 */
import { NgModule }             from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { loginComponent } from "../component/login/login/login.component";
import { findPwdComponent } from "../component/login/find-pwd/find-pwd.component";
import { AuthGuard } from "./auth-guard.service";
import { UserService } from "../service/user.service";

// if(typeof System === "undefined") {
//     var System = {
//         import: function(path) {
//             return Promise.resolve(require(path));
//         }
//     };
// }
const routes:Routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: loginComponent
    },
    {
        path: 'home',
        loadChildren:'./home.module#HomeModule',
        // loadChildren:()=>new Promise(function (resolve) {
        //     (require as any).ensure([], function (require: any) {
        //         resolve(require('./home.module')['HomeModule']);
        //     });
        // }),
        canLoad:[AuthGuard]
        // component: homeComponent
    },
    {
        path: 'table',
        loadChildren:'./data.module#DataModule'
        // loadChildren:()=>new Promise(function (resolve) {
        //     (require as any).ensure([], function (require: any) {
        //         resolve(require('./data.module')['DataModule']);
        //     });
        // })
        // component: DataTablePageDataComponent
    },
    {
        path: 'find_pwd',
        component: findPwdComponent
    },
    {
        path: 'data_calendar',
        loadChildren:'./calendar.module#CalendarPackageModule',
        // loadChildren:()=>new Promise(function (resolve) {
        //     (require as any).ensure([], function (require: any) {
        //         resolve(require('./calendar.module')['CalendarPackageModule']);
        //     });
        // })
    },
    {
        path: 'bi_system',
        loadChildren:'../component/BiModule/bi.module#BiModule',
        canLoad:[AuthGuard]
        // loadChildren:()=>new Promise(function (resolve) {
        //     (require as any).ensure([], function (require: any) {
        //         resolve(require('./calendar.module')['CalendarPackageModule']);
        //     });
        // })
    }
    // , {
    //     path: 'datagrid_playground',
    //     component: PlayGroundComponent
    // }

];

@NgModule({
    imports:[
        RouterModule.forRoot(routes
            ,{preloadingStrategy:PreloadAllModules}
        )
    ],
    providers:[
        AuthGuard,
        UserService
    ],
    exports:[RouterModule]
})
export class AppRoutingModule{}

