/**
 * Created by Yunxuan Yan on 2016/12/16.
 */
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from '../app.component';
import { AppRoutingModule } from "./app-routing.module";
import { LoginModule } from "./login.module";
import { CalendarPackageModule } from './calendar.module';
import { HttpModule } from "@angular/http";
import { Log } from "../service/log.service";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { DataCenterService } from "../service/data-center.service";
import { GlobalService } from "../service/global.service";
import { UserService } from "../service/user.service";
import { PrecompileFormService } from "../service/pre-compile-form.service";
import { WorkflowService } from "../service/workflow.service";
import { DynamicFormService } from "../service/dynamic-form.service";

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        LoginModule,
        HttpModule,
        CalendarPackageModule
    ],
    declarations: [
        AppComponent
    ],
    providers:[
        UserService,
        DataCenterService,
        GlobalService,
        Log,
        PrecompileFormService,
        WorkflowService,
        DynamicFormService,
        { provide: LocationStrategy, useClass: HashLocationStrategy }
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
