/**
 * Created by Yunxuan Yan on 2016/12/16.
 */
import { Injectable }     from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Route, Router, RouterStateSnapshot }    from '@angular/router';
import { UserService } from "../service/user.service";
import { ConfigService } from "../service/config.service";

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private userService:UserService,private router:Router,private configService:ConfigService){}

    canLoad(route:Route):boolean{
        let url = `/${route.path}`;
        return this.checkLogin(url);
    }

    canActivate(route:ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean {
        let url: string = state.url;
        return this.checkLogin(url);
    }

    checkLogin(url:string):boolean {
        this.userService.isLoggin = window.localStorage.getItem("isLoggin")=="1";

        //从cookie判断一下此时是否登出
         if(this.userService.isLoggin){
            return true;
        }

        this.userService.redirectUrl = url;

        this.router.navigate(['/login']);

        return false;
    }
}