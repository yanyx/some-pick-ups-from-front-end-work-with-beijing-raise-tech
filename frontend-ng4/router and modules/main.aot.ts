/**
 * Created by Yunxuan Yan on 2016/12/16.
 */
//在aot机制可运行后可解注本文件

import { AppModuleNgFactory } from '../../compiled/src-webpack/app/modules/app.module.ngfactory';

import { enableProdMode } from '@angular/core';
import { platformBrowser } from "@angular/platform-browser";


if (process.env.ENV === 'production') {
    enableProdMode();
}

platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);