/**
 * Created by Yunxuan Yan on 2017/3/28.
 */

import { ElementRef, Injectable, Renderer } from "@angular/core";
import { GlobalService } from "./global.service";
import { DataGridMissionService } from "./data-grid-mission.service";
import { UserService } from "./user.service";
import { DomHandler } from "../component/vendor/domhandler";
import { DataTableService } from "./dataTable.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PrecompileFormService } from "./pre-compile-form.service";
import {FunctionLib} from './function-lib.service';
import {FloatingFilterComponent} from '../component/data-grid/floating-filter-component/floating-filter.component';

import { DynamicFormService } from "./dynamic-form.service";
@Injectable()
export class DataGridControlService{

    selector = 'DataGrid';
    //datagrid的宽度样式
    chckboxWidth: number  = 30;
    //操作列宽度
    operateColWidth:any=200;
    //全选checkbox状态
    isSelect: boolean = true;
    //是否重置状态（如果是的话那么重置时不会保存当前列宽状态）
    isReset: boolean = false;
    colControlData:any;
    //菜单类型
    menuType: number = 0;
    //tableId
    pageId:number;
    columnEditDefs:any[] = [];
    //关闭窗口时如果重新设置了宽度保存宽度
    widthList: any = {};
    //批量工作流导入成功后返回的id集合
    idsOfBatchList: number[] = [];
    //datagrid数据精度
    accuracy:number;
    //默认初始显示行数
    rows: number = 100;
    //头部字段
    cols:{}[];
    //头部字段属性字典{f1： info}
    colsDict:{}={};
    //字段排序数组
    orderFields:any[] = [];
    //前后台排序
    sortBtn: boolean;
    //隐藏列
    hideColumn: any[] = [];
    //原始表格数据
    originRowData:{};
    dataWidth: number = 120;
    //datagrid提醒的颜色
    remindColor:{};
    //记录dataGrid隐藏列排序以及宽度状态，用来调节
    gridState: any = [];
    //colWidth保存的列宽
    colWidth: any = {};
    //偏好中保存的固定列的数据
    fixCols: any = {
        r:[],
        l:[]
    }    //保存搜索的状态
    searchDataState: any;
    //保存排序状态
    sortDataState: any;
    //子表数据状态
    childInfoState: any;
    //订阅全局搜索参数
    globalFilter;
    //列名称与field对应关系
    colNameToField: any = {};
    //图片编号
    imgNum:number;
    //图片总数
    imgTotal:number;
    //缓存高级查询条件
    cacheCommonFilter:any;
    //表头提醒颜色
    fieldColor: any = {};

    public FloatingFilterComponent = FloatingFilterComponent;

    constructor(
        private globalService: GlobalService,
        private dataGridMissionService: DataGridMissionService,
        private dataTableService:DataTableService,
        private dfService:DynamicFormService,
        private el: ElementRef,
        private renderer: Renderer,
        private precompileFormService:PrecompileFormService,
        private userService:UserService,
        private functionLib:FunctionLib,
        private domHandler:DomHandler
    ){}

    setAccuracy(){
        this.accuracy = UserService.sysConfig.accuracy;
    }

    getColumnControlData(staticForm,modules){
        let colControlData = {};
        this.precompileFormService.preCompileAddingItemForm(staticForm,modules);
        staticForm['data'].forEach((col,index)=>{
            colControlData[col['dfield']]=col;
        });
        this.colControlData = colControlData;
        return;
    }

    setValuesFromCols(){
        let fastSearch = [];
        for( let data of this.cols ){
            this.colNameToField[data['name']] = data['field'];
            if( data['field'] != '_id' && data['dsearch'] == 1 ){
                fastSearch.push( data );
            }
            this.colsDict[data['field']] =data;
        }
        return fastSearch;
    }

    // 设置对应关系的背景色
    setRowStyle(param,correspondenceAddList,correspondenceRemoveList){
        //对应关系的背景色
        if( !param["data"] ){//处理在group中，报错
            return;
        }
        let id = param["data"]["_id"];
        //如果是在工作流进行中的数据显示特殊颜色
        if( param["data"] && param["data"]["status"] && param["data"]["status"] == 2 ){
            return {background:'#E2D6C0'};
        }
        if( param["data"]["data"] && param["data"]["data"]["status"] && param["data"]["data"]["status"] == 1 ){
            return {background:'rgba(255,84,0,.2)'};
        }
        //如果是在工作计算cache中的数据显示特殊颜色
        if( param["data"] && param["data"]["data_status"] && param["data"]["data_status"] == 0 ){
            return {background:'#FFEFEF'};
        }
        if(correspondenceAddList.indexOf(id) != -1){
            //对应关系增加的背景色
            return {background:'#bfda93'};
        }else if(correspondenceRemoveList.indexOf(id) != -1){
            //对应关系减少的背景色
            return {background:'#fd8f8f'};
        }else{
            // return {background:'#fff'};
        }
    }

    verifyCellData(event){
        let newValue = event.newValue;
        if(event['colDef']['required']
            && (newValue == undefined || newValue == '' || (Array.isArray(newValue)&& newValue.length == 0))){
            event.data[event['colDef']['field']] = event.oldValue;
            return  '字段 '+event['colDef']['headerName']+' 不可为空';
        }
        let reg = event['colDef']['reg'];
        if(reg) {
            let r = Object.getOwnPropertyNames(reg)[0];
            let vali = eval(r);
            if (!vali.test(newValue)) {
                // _.gridOptions_top.api.stopEditing(true);//not working
                event.data[event['colDef']['field']] = event.oldValue;
                return reg[r];
            }
        }
        let numArea = event['colDef']['numArea'];
        if(numArea){
            if(newValue < numArea['min'] || newValue > numArea['max']){
                event.data[event['colDef']['field']] = event.oldValue;
                return numArea['error'];
            }
        }
        return null;
    }

    calcColumnState(){
        let indexedGridState = {};
        for(let state of this.gridState) {
            indexedGridState[state['colId']] = state;
        }
        let numState = indexedGridState['number']||{};
        numState['pinned']= this.fixCols.l.length > 0 ? 'left' : null;
        let selectState = indexedGridState['mySelectAll']||{};
        selectState['pinned']= this.fixCols.l.length > 0 ? 'left' : null;
        let oprate = indexedGridState['myOperate']||{};
        let arr = [ numState , selectState , oprate ];
        for( let col of this.fixCols.l ){
            let state = indexedGridState[col]||{};
            state['hide'] = this.hideColumn.indexOf( col ) != -1;
            state['pinned'] = 'left';
            arr.push(state);
        }
        let fixArr = this.fixCols.l.concat( this.fixCols.r );

        for( let data of this.orderFields ){
            if( data == '_id' ){
                continue;
            }
            if( data != 0 && fixArr.indexOf( data ) == -1 ){
                let state = indexedGridState[data]||{};
                state['hide'] = this.hideColumn.indexOf( data ) != -1;
                state['pinned'] = null;
                arr.push(state);
            }
        }
        if(this.orderFields.length == 0){
            for(let state of this.gridState){
                let id = state['colId'];
                if(id != 'number' && id != 'mySelectAll'){
                    state['hide'] = this.hideColumn.indexOf(id)!=-1;
                    state['pinned'] = null;
                    arr.push(state);
                }
            }
        }

        for( let col of this.fixCols.r ){
            let state = indexedGridState[col]||{};
            state['hide'] = this.hideColumn.indexOf( col ) != -1;
            state['pinned'] = 'right';
            arr.push(state);
        }

        let i = 0;
        for( let key in this.colWidth ){
            i++
        }
        if( i != 0 ){
            //先不进行宽度自适应了
            // _this_.emitAutoWidth.emit( false );
        }
        //解决将操作列移到最后
        // let operState = indexedGridState['myOperate']||{};
        // operState['pinned'] = this.fixCols.r.length > 0 ? 'right' : null;
        // arr.push(operState);

        for( let d of arr ){
            if( d.colId && d.colId == 'myOperate' ){
                d['width'] = this.operateColWidth;
            }
        }
        this.gridState = arr;
        return arr;
    }

    judgeType(subRowId,subChildId,subTableType,viewMode,pageId){
        this.pageId = pageId;
        if( subRowId && subChildId ){
            if(subTableType == "child") {
                console.log("加载子表",this.selector);
            } else if (subTableType == "count"){
                console.log( "加载统计",this.selector );
            }
            // this.pageId = subChildId;
        }else {
            console.log(viewMode,this.selector);
            console.log( "加载父表",this.selector );
        }
        return this.pageId;
    }

    createPostData({keyword,subRowId,subChildId,
                       subTableType,subPageId,parentTableId,parentRealId,parentTempId,
                       queryParams, viewMode,rowId,tableType,fieldId, recordId,
                       isBatch,tempIdsForBatchInApprove, sourceTableId,
                       tableIdForBuildin,baseBuildinDfield,isShowGroup}){
        let postData = {
            table_id: this.pageId,
            first: 0,
            rows: this.rows
        };
        if(!postData['rows']){
            postData['rows']=100;
        }
        if(keyword){
            postData['tableType']='keyword';
        }
        if (subRowId && subChildId) {
            if(subTableType=="child"){
                console.log("加载子表",this.selector);
                postData["childInfo"]= {"parent_page_id": subPageId, "parent_row_id": subRowId};
                postData["parent_table_id"]=parentTableId;
                postData["parent_real_id"]=parentRealId;
                postData["parent_temp_id"]=parentTempId;
            } else if(subTableType == "count"){
                console.log("加载统计",this.selector);
                postData["childInfo"]={"parent_page_id": subPageId, "parent_row_id": subRowId};
            }
        } else {
            postData["parent_table_id"]=parentTableId;
            postData["parent_real_id"]=parentRealId;
            if (parentTableId == "") {
                postData["parent_temp_id"]=parentTempId;
                console.log("加载正常表单",this.selector);
                if(queryParams['_id']){
                    postData["mongo"]=queryParams;
                }
            } else {
                switch (viewMode){
                    case 'viewFromSongrid':
                        postData["rowId"]=rowId;
                    case 'editFromSongrid':
                        postData["parent_temp_id"]=parentTempId;
                        postData["childInfo"]={"parent_page_id": parentTableId,"parent_row_id": parentRealId || parentTempId};
                        break;
                    case 'viewFromCorrespondence':
                        //如果来自对应关系
                        postData["parent_temp_id"]=parentTempId;
                        postData["is_temp"]=0;
                        postData["rowId"]=rowId;
                        postData['rows']=9999;
                        break;
                    case 'editFromCorrespondence':
                        postData["parent_temp_id"]=parentTempId;
                        postData["is_temp"]=1;
                        postData["rowId"]=rowId;
                        postData['rows']=9999;
                        break;
                    default:
                        var myId = (parentTempId != "")?parentTempId:parentRealId;
                        postData["rowId"]=rowId;
                        postData["tableType"]=tableType;
                        postData["fieldId"]=fieldId;
                        postData["childInfo"]={"parent_page_id": parentTableId,"parent_row_id": myId || parentRealId};
                        if (parentTempId) {
                            postData["parent_temp_id"]=parentTempId;
                        }
                }
            }
        }
        if(recordId){
            postData['record_id']=recordId;
        }
        //如果是批量审批
        if(isBatch == '1'){
            postData = this.setBatch(tempIdsForBatchInApprove,isBatch);
        }
        if( tableType == 'count' ){
            delete postData['childInfo'];
        }
        this.getSearchAndSort(postData,tableType,keyword,isShowGroup);
        //子表情况下首次进入不加载常用查询
        if( parentTableId ){
            postData['is_filter'] = 1;
        }

        if( tableType == 'source_data' ){
            postData['source_table_id'] = sourceTableId;
            postData['table_id'] = tableIdForBuildin;
            postData['base_buildin_dfield'] = baseBuildinDfield;
            postData['filter'] = {'_id':rowId}
            postData['is_filter'] = 1;
            postData['tableType'] = tableType;
        }
        if( postData['childInfo'] ){
            this.childInfoState = postData['childInfo'];
        }
        return postData;
    }

    setBatch(tempIdsForBatchInApprove,isBatch){
        let $in;
        if (tempIdsForBatchInApprove) {
            if (tempIdsForBatchInApprove.length > 0) {
                $in = tempIdsForBatchInApprove
            }else if (this.idsOfBatchList) {
                $in = this.idsOfBatchList;
            }
        } else if (this.idsOfBatchList) {
            $in = this.idsOfBatchList;
        };
        return {
            table_id: this.pageId,
            first: 0,
            rows: this.rows,
            is_batch: isBatch,
            mongo: {
                _id: { $in: $in }
            }
        };
    }

    //看是否有保存的搜索或者排序数据有的话拼上去
    getSearchAndSort (postData,tableType,keyword,isShowGroup,myGroup?){
        if( tableType == 'count' ){
            delete postData['childInfo'];
        }
        //搜索数据
        if( this.searchDataState ){
            postData['filter'] = this.searchDataState;
        }else {
            delete postData['filter'];
        }
        //高级查询
        if( this.cacheCommonFilter ){
            postData['filter'] = this.cacheCommonFilter;
            if( this.searchDataState ){
                postData['filter'] = [];
                postData['filter'] = postData['filter'].concat( this.cacheCommonFilter );
                postData['filter'] = postData['filter'].concat( this.searchDataState );
            }
        }
        //排序数据
        if( this.sortDataState && this.sortDataState.sortField && this.sortDataState.sortOrder ){
            postData['sortOrder'] = this.sortDataState.sortOrder;
            postData['sortField'] = this.sortDataState.sortField;
            postData['sort_real_type'] = this.sortDataState.sort_real_type;
        }else {
            delete postData['sortOrder'];
            delete postData['sortField'];
            delete postData['sort_real_type'];
        }
        //子表状态
        if( this.childInfoState ){
            postData['childInfo'] = this.childInfoState;
        }
        if(keyword && this.globalFilter){
            postData['keyWord'] = this.globalFilter;
            postData['tableType']='keyword';
        }else{
            delete  postData['keyWord'];
            delete  postData['tableType'];
        }
        if( tableType ){
            postData['tableType'] = tableType;
        }else {
            delete postData['tableType'];
        }
        //分组状态
        if( isShowGroup ){
            postData['tableType'] = 'group';
            postData['is_group'] = 1;
            postData['group_fields'] = myGroup;
        }else {
            postData['tableType'] = tableType;
            delete postData['is_group'];
            delete postData['group_fields'];
        }
        //全局搜索(警告：这个需要放在最后执行注意tableType要不然全局搜索传参出问题)
        if(keyword && this.globalFilter){
            postData['keyWord']=this.globalFilter;
            postData['tableType']='keyword';
        }
        return postData;
    }

    abjustAfterView(){
        setTimeout(function () {
            if($("#detail-table .ui-myformtable .ag-body-viewport").length != 0 || $("#collapseForm .ui-myformtable .ag-body-viewport").length != 0) {}
            else
            {
                if($('correspondence-control').length!=0)return;
                if($(".ui-tab-selected").get(0).title=='人员信息')return;
                $("#myGrid .ag-body-viewport").attr("tabindex",1);
                $("#myGrid .ag-body-viewport").css('outline','none');
                $("#myGrid .ag-body-viewport").focus();
            }
        },2000);
    }

    initRowData(resRows,parentTableId,songridFromInline){
        this.originRowData = {};
        if (parentTableId !== "" && songridFromInline == '1') {
            //父表中填写的数据
            let dict = this.globalService.frontendParentFormValue[parentTableId];
            //父子数据字段对应的关系 {f1: f2 ,temp_id:section_page_id }
            let kvDict = [];
            try{kvDict = this.globalService.frontendRelation[parentTableId][this.pageId]["pdfield_2_cdfield"];}
            catch (err){
                //console.error(err)
            }
            //组装子表所需列表或表单中内置或相关的父表中数据
            let parentData = this.globalService.packageParentDataForChildData(kvDict, dict, parentTableId);
            for (let item of resRows) {
                for (let key in kvDict) {
                    let childDfield = kvDict[key];
                    if (parentData.hasOwnProperty(key) && item.hasOwnProperty(childDfield)) {
                        item[childDfield] = parentData[key];
                    }
                }
            }
        }
        let rowData = resRows;
        //originRowData用深拷贝，保存初始值
        rowData.forEach((row,index)=>{
            this.originRowData[row['_id']]=JSON.parse(JSON.stringify(row));
        });
        return rowData;
    }

    selectedRows(node,theArr) {
        if( !node["data"] ){//处理在group中，报错
            return;
        }
        let id = node["data"]["_id"];
        for(let i=0;i<theArr.length;i++){
            if(theArr[i]==id){
                node.setSelected(true);
            }
        }
    }

    createFooterData(res, AggList,columnDefs_bottom){
        //获得fooetr同时获取分组函数相关数据
        AggList = [];
        if( res.rows && res.rows[0] ){
            for( let key in res.rows[0] ){
                AggList.push( key );
            }
        }
        let arr = [];
        let obj = res.rows[0] || {};
        obj["myfooter"] = '合计';
        arr.push(obj);
        let rowData_footer = arr;
        //处理footer提示undefine
        this.clearFooterUndefined(columnDefs_bottom,rowData_footer);
        return rowData_footer;

    }

    //footer排除undefined的数据
    clearFooterUndefined(columnDefs_bottom, rowData_footer) {
        let arr = [];
        if(columnDefs_bottom){
            for (let data of columnDefs_bottom) {
                if (["myGroups", "myOperate", "mySelectAll", "myOperate_1", "number"].indexOf(data["field"]) == -1) {
                    arr.push(data["field"]);
                }
            }
            let arrLength = arr.length;
            for (let i = 0; i < arrLength; i++) {
                if ( rowData_footer[0][arr[i]] === undefined || rowData_footer[0][arr[i]] === NaN ) {
                    rowData_footer[0][arr[i]] = '';
                }
            }
        }
    }

    //根据数据数量选择排序方式
    sortWay(total) {
        if (total > this.rows) {
            this.sortBtn = false;
            console.log("后台排序",this.selector)
        } else {
            this.sortBtn = true;
            console.log("前台排序",this.selector)
        }
        //如果有大数字段设置为后端排序
        for( let d of this.cols ){
            if( d['real_type'] && d['real_type'] == 26 ){
                this.sortBtn = false;
                console.log("有大数字段后台排序",this.selector)
            }
        }
        return this.sortBtn;
    }

    getContextMenuItems(params) {
        let result = [
            'copy',
            'paste',
            {
                name: '行选择',
                action: ()=>{
                    //实现右键选择行
                    let selectRange = params.api.getRangeSelections();
                    for( let data of selectRange ){
                        params.api.forEachNode( function (node) {
                            if( node.childIndex >= data["start"]["rowIndex"] && node.childIndex <= data["end"]["rowIndex"] ){
                                node.setSelected(!node.selected, false);
                            }
                        });
                    }
                },
            }
        ];
        return result;
    }

    setFontStyle(){
        //设置字体为保存的字号
        try{
            if(localStorage != undefined){
                if(UserService.userInfo!=undefined){
                    let str = UserService.userInfo.username+'-stylelocal';
                    let stylelocal = (JSON.parse(localStorage.getItem(str)))["style"];
                    var stylenow;
                    for (let s of stylelocal) {
                        if (s.isSelected == '1') {
                            stylenow = s;
                        }
                    }
                    if (stylenow.gridfont != undefined) {
                        return stylenow.gridfont + 'px';
                    }
                }
                return null;
            }
        }catch(err){ return null;}
    }

    //表单columns
    createHeaderColumnDefs(emitReset,oHandle,
                           isFixed,isEdit,customOperateList,
                           rowOperation,fieldContent,
                           tableType,source_field_dfield,
                           viewMode,viewOrEdit?:boolean,editCols?:boolean){
        let columnDefs = [];
        let headerArr:any = [];
        let columnArr:any = [];
        let otherCol :any= [];
        let This = this;
        for (let col of this.cols) {
            headerArr.push({ data:col, header:col["name"].split('->') });
        }
        for( let data of headerArr ){
            for( let i = 0,length = data.header.length;i<length;i++ ){
                this.getArr(i,0,columnArr,length,data,viewOrEdit,
                    otherCol,fieldContent,
                    tableType,source_field_dfield,
                    viewMode,editCols);
            }
        }
        this.orderFields = this.orderFields.concat(otherCol);
        for (let col of columnArr) {
            columnDefs.push(col);
        }
        //解决定制列将数据列拖拽到操作列和全选列之间导致全选列不显示的BUG(将这些选项移动到后边)
        columnDefs.unshift(
            {
                // headerCellTemplate: (params) => {
                //     return This.selectAll(params, This);
                // },
                headerName: "",
                width: This.chckboxWidth,
                checkboxSelection: true,
                colId: "mySelectAll",
                hide: false,
                field: "mySelectAll",
                suppressSorting: true,
                suppressResize: true,
                suppressMovable: true,
                suppressMenu: true,
                headerCheckboxSelection: true,
                supressToolPanel: true,
                suppressFilter: true,
                cellStyle: {
                    'padding-left': '0px',
                    'padding-right': '0px',
                    'text-align': 'center',
                    'font-style': 'normal'
                },
                headerClass:'header-select'
            }
        );
        let obj = {
            headerName: '操作',
            colId: "myOperate",
            hide: false,
            field: "myOperate",
            suppressSorting: true,
            suppressResize: false,
            suppressMovable: false,
            suppressFilter: true,
            suppressMenu: true,
        }
        if( this.menuType ){
            obj["headerCellTemplate"]=This.addDiv;
            obj["width"]=This.chckboxWidth;
            obj["cellStyle"]={'padding': '0px', 'font-style': 'normal', 'overflow': 'visible'};
            obj["cellRenderer"]= function(params) { return This.handleCellRenderer1(params,oHandle); };
        }else{
            obj["width"]=this.operateColWidth;
            obj["cellStyle"]={'font-style': 'normal'};
            obj["cellRenderer"]=function(params){
                return This.handleCellRenderer(params, viewMode,isFixed,isEdit,customOperateList,rowOperation);
            };
        }
        columnDefs.push(obj);
        columnDefs.unshift(
            {
                //生成编号
                cellRenderer: (params)=>{
                    if( params.data&&params.data.myfooter&&params.data.myfooter == '合计' ){
                        return '合计';
                    }
                    return '<span style="text-align: center;font-size: 12px;display: block;">' + ( params.rowIndex + 1 ) + '</span>';
                },
                headerCellTemplate: (params) => {
                    return This.resetPreference( params, emitReset);
                },
                headerName: '序号',
                colId: "number",
                hide: false,
                field: "number",
                width: 30,
                headerClass:'ag-grid-number',
                suppressSorting: true,
                suppressResize: true,
                suppressMovable: true,
                suppressMenu: true,
                suppressFilter: true,
                cellStyle: {
                    'text-align': 'center',
                    'padding': '0px'
                }
            }
        );
        columnDefs.unshift(
            { headerName: '分组', field: 'group' ,pinned:'left',hide:true,suppressSorting: true,suppressMovable:true,cellRenderer: 'group', suppressMenu: true, tooltipField:'group' }
        )
        // setTimeout(()=>{
        //     if(this.columnEditDefs.length == 0){
        //         //可编辑的列定义初始化
        //         for(let col of columnDefs){
        //             this.setEditableCol(col);
                // }
        //         //检查一遍是否有依赖于其他字段的字段具有限定条件,暂时没有做验证
        //         // for(let col of this.columnEditDefs){
        //         //     let controlData = this.colControlData[col['colId']];
        //         //     console.dir(controlData);
        //         // }
        //         // console.dir(this.columnEditDefs);
        //     }
        // });
        return columnDefs;
    }

    //生成多级表头的数据结构
    getArr(i,n,column,len,data,viewOrEdit,
           otherCol,fieldContent,
           tableType,source_field_dfield,
           viewMode,editCols){
        if( i == n ){
            this.header( column,i,len,data ,viewOrEdit,
                otherCol,fieldContent,
                tableType,source_field_dfield,
                viewMode,editCols)
        }else{
            for( let col of column ){
                if( data.header[n] == col['headerName'] && col['children']){
                    this.getArr( i,n+1,col['children'],len,data,viewOrEdit,
                        otherCol,fieldContent,
                        tableType,source_field_dfield,
                        viewMode,editCols);
                }
            }
        }
    }

    //生成多级表头的数据结构
    header( column,i,len,data ,viewOrEdit,
            otherCol,fieldContent,
            tableType,source_field_dfield,viewMode,editCols){
        let key = 0;
        for( let col of column ){
            if( col['headerName'] == data.header[i] ){
                key++;
            }
        }
        if( key == 0 ){
            if( i != ( len - 1 ) ){
                column.push( {
                    headerName: data.header[i],
                    groupId: data.header[i],
                    marryChildren: false,
                    children: []
                } )
            }else{//多级表头最底层节点，作为一列
                let This = this;
                let img1 = require('../component/data-grid/icon_paixu_1.png');
                let img2 = require('../component/data-grid/icon_paixu_2.png');
                //id列不添加多级索引不添加
                if (data.data["field"] == "_id" || data.data['dtype'] == 9) {
                    return;
                }
                let headClass = This.numOrText(data.data["real_type"])?'header-style-r':'header-style-l';
                //解决后台配置字段之后类排序没有该字段导致该列不显示的BUG
                if (this.orderFields.indexOf(data.data["field"]) == -1) {
                    otherCol.push(data.data["field"]);
                }
                let fixArr = this.fixCols.l.concat( this.fixCols.r );
                let obj={
                    headerName: data.header[i],
                    headerCellTemplate: (params) => {
                        return this.headerCellRenderer(params);
                    },
                    // headerComponent:HeaderComponent,
                    // headerComponentFramework:<{new():HeaderComponent}>HeaderComponent,
                    tableName: data.data['table_name'],
                    id: data.data["id"],
                    field: data.data["field"],
                    enableCellChangeFlash: true,
                    suppressMenu: true,
                    // suppressToolPanel: true,
                    // width: 160,
                    suppressMovable: fixArr.indexOf( data.data["field"] ) == -1 ? false : true,
                    field_content: data.data['field_content'],
                    colId: data.data["field"],
                    source_table_id: data.data["source_table_id"] || '',
                    base_buildin_dfield: data.data["base_buildin_dfield"] || '',
                    source_field_dfield: data.data["source_field_dfield"] || '',
                    source_table_name: data.data["source_table_name"] || '',
                    is_correspondence_num: data.data["is_correspondence_num"] || 0,
                    dtype: data.data["dtype"],
                    dsearch: data.data["dsearch"],
                    child_table_id: data.data["child_table_id"],
                    count_table_id: data.data["count_table_id"],
                    dinput_type: data.data["dinput_type"],
                    is_user: data.data["is_user"],
                    main_type: data.data["main_type"],
                    real_type: data.data["real_type"],
                    real_accuracy:data.data["real_accuracy"],
                    tooltipField: (data.data["dinput_type"] == 23 ||data.data["dinput_type"] == 2)? '' : data.data["field"],
                    sortingOrder: ['desc', 'asc', null],
                    hide: false,
                    minWidth: 110,
                    filter: this.numOrText(data.data["real_type"]) ? "number" : "text",
                    headerClass: headClass,
                    cellStyle: {'font-style': 'normal'},
                    cellRenderer: (params) => {
                        return This.bodyCellRenderer(params,tableType,source_field_dfield,viewMode,This);
                    },
                    floatingFilterComponentFramework: FloatingFilterComponent,
                    // floatingFilterComponent: FloatingFilterComponent,
                    floatingFilterComponentParams:{
                        suppressFilterButton: true,
                        colInfo: data.data,
                        searchOldValue: this.searchOldValue,
                        searchValue: this.searchValue
                    },
                    enableRowGroup: true,
                    enableValue: true,
                    icons: {
                        sortAscending: '<img src="' + img1 + '" style="width: 15px;height:15px;"/>',
                        sortDescending: '<img src="' + img2 + '" style="width: 15px;height:15px;"/>'
                    }
                };
                if (fieldContent) {
                    if (data.data['id'] == fieldContent['child_field']||data.data['id'] == fieldContent['count_field']) {
                        obj['cellStyle'] = {'font-style': 'normal'};
                        obj['cellStyle']['background-color'] =  'rgb(177,215,253)';
                    }
                }
                // 图片可见单元格属性修改
                if (data.data["dinput_type"] == 23 && data.data['field_content']['is_show_image'] == 1) {
                    obj['cellStyle'] = { 'font-style': 'normal'};
                    obj['cellStyle']['overflow'] = "visible";
                }
                let width = data.data["width"];
                if( this.colWidth && this.colWidth[data.data["field"]] ){
                    width = this.colWidth[data.data["field"]];
                }
                obj["width"] = width;
                if (( this.childTable(data.data["dinput_type"]) || this.countTable(data.data["dinput_type"]) ) && viewOrEdit) {
                    obj['editable'] = false;
                    obj['cellStyle'] = {'font-style': 'normal'};
                    obj['cellStyle'] ['background']= "#ddd"
                }
                if( editCols ){
                    this.setEditableCol( obj );
                }
                column.push(obj) ;
            }
        }
    }
    isShowEditCancel:boolean=false;
    headerCellRenderer(params){
        let field = params.column.colDef.field;
        let img1 = require('../component/data-grid/icon_paixu_1.png');
        let img2 = require('../component/data-grid/icon_paixu_2.png');
        let color = '';
        if( this.fieldColor ){
            for( let c in this.fieldColor ){
                if( this.fieldColor[c].indexOf( field )!=-1 ){
                    color = c;
                    break;
                }
            }
        }
        if( color != '' ){
            let rgc = this.colorRgb(color,0.7);
            // return '<div style="width: 100%;display: inline-block;height: 100%;background-color: '+ rgc +';outline: 10px solid '+ rgc +';">'+ params.value +'</div>';
            return `<div class="ag-header-cell header-style-l ag-header-cell-sortable ag-header-cell-sorted-none" colid="`+ field +`"
            style="width: 100%;display: inline-block;height: 100%;background-color: `+ rgc +`;">
            <div id="agResizeBar" class="ag-header-cell-resize"></div>
            <div id="agHeaderCellLabel" class="ag-header-cell-label"><span id="agSortAsc"
        class="ag-header-icon ag-sort-ascending-icon ag-hidden"><img
                src="`+ img1 +`"
            style="width: 15px;height:15px;"></span> <span id="agSortDesc"
        class="ag-header-icon ag-sort-descending-icon ag-hidden"><img
                src="`+ img2 +`"
            style="width: 15px;height:15px;"></span> <span id="agNoSort"
        class="ag-header-icon ag-sort-none-icon ag-hidden"><svg
                width="10" height="10"><polygon points="0,4 5,0 10,4"></polygon><polygon
                points="0,6 5,10 10,6"></polygon></svg></span> <span id="agFilter"
        class="ag-header-icon ag-filter-icon ag-hidden"><svg
                width="10" height="10"><polygon points="0,0 4,4 4,10 6,10 6,4 10,0" class="ag-header-icon"></polygon></svg></span>
            <span id="agText" class="ag-header-cell-text">`+  +`</span></div>
                </div>`
        }
    }

    //body表单columns  cellRenderer
    bodyCellRenderer(params,tableType,source_field_dfield,viewMode,This) {
        if( params.data&&params.data.myfooter&&params.data.myfooter == "合计" ){
            return params.value || '';
        }
        let myValue = params['value'];  //当前单元格数值
        let remindColorInfo = this.remindColor['remind_color_info'];//枚举提醒颜色
        let info = this.remindColor['info'];//数字提醒颜色
        let colDef = params.colDef;         //当前单元格数据
        let rowId;     //当前rowId
        let sHtml;      //要显示的html元素的字符串
        let color = "transparent"; //颜色
        if (params.data) {
            rowId = params.data['_id']
        }
        //显示提醒颜色start---
        //判断是否为数字类型
        try {
            if(info){
                let oInfo = JSON.parse(info);
                for (let i in oInfo) {
                    if (i == rowId) {
                        // if (oInfo[i].indexOf(colDef['colId']) != -1) {
                            try{
                                color = oInfo[i][colDef['colId']];
                                // color = remindColorInfo[colDef['colId']]['color'];
                            }catch (err){
                                color = 'transparent';
                            }
                        // }
                    }
                }
            }
            //判断是否为枚举类型
            for (let i in remindColorInfo) {
                if (i == colDef['colId']&&remindColorInfo[i][myValue]) {
                    color = remindColorInfo[i][myValue];
                }
            }
        }catch (err){

        }

        let opcity='0.3';
        if(color != undefined && color != 'transparent'){
            color= This.colorRgb(color,opcity);
        }
        //是否是重要字段（入库之前检测下是否为空）
        if(colDef.main_type && colDef.main_type.id != null){
            //兼容日期规则控件
            if(myValue=='' || myValue==undefined ||(params.value[-1]!=undefined && params.value[-1]=='')){
                let conditionAll=colDef.main_type["condition"];
                for(let condition of conditionAll) {
                    let conditinField = '';
                    if (condition["optionfield"] != null) {
                        //通过id查找field
                        for (let col of this.cols) {
                            if (col["id"] == condition["optionfield"]) {
                                conditinField = col["field"];
                            }
                        }
                        //条件字段的匹配条件（==，>,<,>=,<=）
                        switch (condition["option"]) {
                            case '==': {
                                if (params.data[conditinField] == condition["context"]) {
                                    color = 'rgba(255,0,0,1)'
                                }
                                break;
                            }
                            case '>': {
                                if (params.data.conditinField > condition["context"]) {
                                    color = 'rgba(255,0,0,1)'
                                }
                                break;
                            }
                            case '<': {
                                if (params.data.conditinField < condition["context"]) {
                                    color = 'rgba(255,0,0,1)'
                                }
                                break;
                            }
                            case '<=': {
                                if (params.data.conditinField <= condition["context"]) {
                                    color = 'rgba(255,0,0,1)'
                                }
                                break;
                            }
                            case '>=': {
                                if (params.data.conditinField >= condition["context"]) {
                                    color = 'rgba(255,0,0,1)'
                                }
                                break;
                            }
                        }
                    } else {
                        color = 'rgba(255,0,0,1)';
                    }
                }
            }
        }
        if( tableType == 'source_data' && source_field_dfield == colDef.field ){
            color='rgba(255,0,0,0.5)';
        }
        if (params.value == undefined) {
            sHtml = '<span style="min-height: 28px;text-align: center;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"></span>';
            return sHtml;

        }
        //前端表达式值计算
        if(colDef.dinput_type==22){
            let exp = colDef.field_content.update_exp;
            let row_data = params.data;
            let reg = /\@f\d+\@/g;
            let items = exp.match(reg);
            for (let i in items){
                i = items[i].replace("@", "").replace("@", "");
                let is_number = (this.colsDict[i]&&(this.colsDict[i].real_type==10||this.colsDict[i].real_type==11));
                let f = i;
                i = "@"+i+"@";
                while(exp.indexOf(i)!=-1){
                    // this.cols
                    exp = exp.replace(i, is_number?row_data[f]:"'"+row_data[f]+"'");
                }
            }
            exp = exp.replace("#", "this.functionLib.");
            while (exp.indexOf("#")!=-1){
                exp = exp.replace("#", "this.");
            }
            params["value"] = eval(exp);
        }
        if(this.isShowEditCancel && params.colDef && !params.colDef.editable){
            color='rgba(230,230,230,0.8)';
        }
        if (this.numOrText(colDef["real_type"])) {  //数字类型
            if (this.intOrFloat(colDef["real_type"])) {//判断整数和小数
                if (this.childTable(colDef['dinput_type']) || this.countTable(colDef['dinput_type'])) { //子表或统计类型
                    if(viewMode == 'viewFromCorrespondence' || viewMode == 'editFromCorrespondence'){
                        // sHtml = '<span style="float:right;color:rgb(85,85,85);">' + this.formatter(this.accuracy, params.value) + '<span/>';
                        sHtml = '<span style="text-align:right;margin: -5px;color:rgb(85,85,85);padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>' + this.formatter(params.value) + '</span><span/>';
                    }else{
                        // sHtml = '<a style="float:right;color:#337ab7" id="childOrCount">' + this.formatter(this.accuracy, params.value) + '</a>';
                        sHtml = '<a style="text-align:right;margin: -5px;color:#337ab7;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span id="childOrCount">' + this.formatter(params.value) + '</span></a>';
                    }
                } else {
                    if( colDef['base_buildin_dfield'] !='' && colDef['source_table_id'] != '' && colDef['headerName'] != '创建人' && colDef['headerName'] != '最后修改人' ){
                        sHtml = '<a  title="查看源数据" style="text-align:right;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span id="relatedOrBuildin">' + this.formatter(params.value) + '</span><span/>';
                    }else {
                        sHtml = '<span style="text-align:right;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>' + this.formatter(params.value) + '</span><span/>';
                    }
                }
            } else {//小数
                if (this.childTable(colDef['dinput_type']) || this.countTable(colDef['dinput_type'])) { //子表或统计类型
                    if(viewMode == 'viewFromCorrespondence' || viewMode == 'editFromCorrespondence'){
                        // sHtml = '<span style="float:right;color:rgb(85,85,85);">' + this.formatter(this.this.accuracy, Number(params.value).toFixed(colDef.real_accuracy)) + '<span/>';
                        sHtml = '<span style="text-align:right;margin: -5px;color:rgb(85,85,85);padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '</span><span/>';
                    }else{
                        // sHtml = '<a style="float:right;color:#337ab7" id="childOrCount">' + this.formatter(this.accuracy, Number(params.value).toFixed(colDef.real_accuracy)) + '</a>';
                        sHtml = '<a style="text-align:right;color:#337ab7;margin: -5px;color:rgb(85,85,85);padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span  id="childOrCount">' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '</span></a>';
                    }
                }else {
                    if( colDef['base_buildin_dfield'] !='' && colDef['source_table_id'] != '' && colDef['headerName'] != '创建人' && colDef['headerName'] != '最后修改人' ){
                        sHtml = '<a  title="查看源数据" style="text-align:right;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span id="relatedOrBuildin">' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '</span><span/>';
                    }else {
                        sHtml = '<span style="text-align:right;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '</span><span/>';
                    }
                }
            }
        } else if (colDef["real_type"] == 9) {   //附件类型附件名转换编码
            try {
                sHtml = '<span style="color: rgb(85,85,85);">' + decodeURI(params.value) + '</span>';
            } catch (err) {
                sHtml = '<span style="color:rgb(85,85,85);">' + params.value + '</span>';
            }
        } else if (colDef["real_type"] == 25) {
            sHtml = '<span style="text-align: center;">******</span>';
        } else if(colDef["real_type"] == 27){
            let val = params["data"][colDef["colId"]]["-1"] || "";
            if(val !== "") {
                val = val.replace(/\n/g,"\n;\n");
            }
            if( colDef['base_buildin_dfield'] !='' && colDef['source_table_id'] != '' && colDef['headerName'] != '创建人' && colDef['headerName'] != '最后修改人' ){
                sHtml = '<a  title="查看源数据" style="text-align: center;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span id="relatedOrBuildin">'+ val +'</span></a>';
            }else {
                sHtml = '<span style="text-align: center;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>'+ val +'</span></span>';
            }
        }else if (colDef["real_type"] == 2) {   //富文本编辑框
            try {
                sHtml = '<a style="text-align: center;">查看详情</a>';
            } catch (err) {
                sHtml = '<a style="text-align: center;">查看详情</a>';
            }
        }else if( colDef["real_type"] == 26 ){//大数
            if (this.childTable(colDef['dinput_type']) || this.countTable(colDef['dinput_type'])) {
                if(params.value > 9007199254740992){
                    sHtml = '<a style="float:right;color:#337ab7;" id="childOrCount">'+this.formatter(params.value.toString())+'.00'+'</a>';
                }else{
                    sHtml = '<a style="float:right;color:#337ab7;" id="childOrCount">' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '<span/>';
                }
            }else {
                if(params.value > 9007199254740992){
                    if( colDef['base_buildin_dfield'] !='' && colDef['source_table_id'] != '' && colDef['headerName'] != '创建人' && colDef['headerName'] != '最后修改人' ){
                        sHtml = '<a  title="查看源数据" style="margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span id="relatedOrBuildin">'+this.formatter(params.value.toString())+'.00'+'</span></a>';
                    }else {
                        sHtml = '<span style="margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>'+this.formatter(params.value.toString())+'.00'+'</span></span>';
                    }
                }else{
                    if( colDef['base_buildin_dfield'] !='' && colDef['source_table_id'] != '' && colDef['headerName'] != '创建人' && colDef['headerName'] != '最后修改人' ){
                        sHtml = '<a  title="查看源数据" style="margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span id="relatedOrBuildin">' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '</span><span/>';
                    }else {
                        sHtml = '<span style="margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '</span><span/>';
                    }
                }
            }
        }else if( colDef["real_type"] == 29 ){//地址类型
            sHtml = '<a href="'+myValue+'" style="float:left;color:#337ab7;" id="shareAddress" target="_blank">'+myValue+'</a>';
        }else if( colDef["real_type"] == 34 ) {//合同编辑器
            sHtml = '<a style="text-align: center;color:#337ab7;">' + "查看" + '</a>' + '<span style="color:#000">' + "丨" + '</span>' + '<a style="text-align: center;color:#337ab7;">' + '下载' + '</a>';
        }else { //除数字外，都是文本类型
            if (this.childTable(colDef.dinput_type) || this.countTable(colDef.dinput_type,colDef['real_type'])) { //子表或统计类型
                if(viewMode == 'viewFromCorrespondence' || viewMode == 'editFromCorrespondence'){
                    sHtml = '<span style="float:right;color:rgb(85,85,85);">' + params.value + '</span>';
                }else {
                    // sHtml = '<a style="float:right;color:#337ab7" id="childOrCount">' + params.value + '</a>';
                    sHtml = '<a style="text-align:right;margin: -5px;color:#337ab7;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '" ><span id="childOrCount">' + params.value + '</span></a>';
                }
            } else {
                if( colDef['base_buildin_dfield'] !='' && colDef['source_table_id'] != '' && colDef['headerName'] != '创建人' && colDef['headerName'] != '最后修改人' ){
                    sHtml = '<a  title="查看源数据" style="margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span id="relatedOrBuildin">' + params.value + '</span></a>';
                }else {
                    sHtml = '<span style="min-height: 28px;margin: -5px;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + '"><span>' + params.value + '</span></span>';
                }
            }
        }
        //不显示数字的对应关系
        if( colDef["real_type"] == 16){
            if( viewMode == 'editFromCorrespondence'){
                sHtml = '<span style="color:' + color + '">' + params.value + '</span>';
            }else{
                sHtml = '<a style="text-align:right;margin: -5px;color:#337ab7;padding-left: 5px;padding-right: 5px;display: inline-block;width: calc(100% + 10px);height: 100%;background-color:' + color + ' " ><span id="correspondenceClick">' + params.value + '</span></a>';
            }
        }
        if (colDef["dinput_type"] == 23 && colDef['field_content']['is_show_image'] == 1) {
            let cDiv = document.createElement('div');
            let cImg = document.createElement('img');
            let dImg = document.createElement('img');
            cImg.src = params.value;
            cImg.style.height = 'inherit';
            cImg.style.position = 'relative';
            cDiv.style.height = 'inherit';
            dImg.style.position = 'absolute';
            dImg.style.maxWidth = (window.innerWidth / 2 - 120) + 'px';
            dImg.style.maxHeight = (window.innerHeight - 300) / 2 + 'px';
            dImg.style.zIndex = '2';
            cImg.addEventListener("mouseover", function (param) {
                dImg.src = param.target['src'];
                if (param.y > window.innerHeight / 2) {
                    dImg.style.bottom = '0'
                }
                if (param.x < window.innerWidth / 2) {
                    dImg.style.left = '100%'
                } else {
                    dImg.style.right = '100%'
                }
                cDiv.appendChild(dImg);

            });
            cImg.addEventListener("mouseout", function () {
                cDiv.removeChild(dImg);
            });
            cDiv.appendChild(cImg);
            sHtml = cDiv;
        }
        if(colDef['real_type']==9||colDef["real_type"] == 33){
            sHtml = '<a id="file_view" title="查看详情">'+myValue.length+'个附件</a>';
        }
        //分组列设置
        if( params&&params.colDef&&params.colDef.headerName=='Group' ){
            sHtml = sHtml = '<span>' + params.value + '</span>';
        }
        return sHtml;
    }

    /*16进制颜色转为RGB格式*/
    colorRgb(str,opcity){
        var sColor = ''+str.toLowerCase();
        if(sColor){
            if(sColor.length === 4){
                var sColorNew = "#";
                for(var i=1; i<4; i+=1){
                    sColorNew += sColor.slice(i,i+1).concat(sColor.slice(i,i+1));
                }
                sColor = sColorNew;
            }
            //处理六位的颜色值
            var sColorChange = [];
            for(var i=1; i<7; i+=2){
                sColorChange.push(parseInt("0x"+sColor.slice(i,i+2)));
            }
            return "rgba(" + sColorChange.join(",")+","+opcity + ")";
        }else{
            return sColor;
        }
    };

    //生成操作项
    handleCellRenderer1(params,oHandle) {
        let This = this;

        // let oHandle = This.domHandler.findSingle( this.el.nativeElement,'div.data-grid-handle' );
        let oDiv = document.createElement('div'),
            img1 = require('../component/data-grid/icon_handle.png');
        oDiv.style.width = '100%';
        oDiv.style.height = '100%';
        oDiv.style.background = 'url("' + img1 + '") no-repeat center center';
        oDiv.addEventListener('click', function (event) {
            oHandle.style.left = '48px';
            if (oDiv.children.length == 0 || oHandle.style.display == 'none') {
                oDiv.appendChild(oHandle);
                oHandle.style.display = 'block';
                let num = /\d+/;
                let size = parseInt($(oHandle).css('font-size').match(num)[0]);
                oHandle.style.top = (20 - size) / 2 + 'px';
                oHandle.style.lineHeight = size + 6 + 'px';
                let icon = This.domHandler.findSingle(oHandle, 'i.icon-handle-arrow');
                icon.style.top = (size - 4) / 2 + 'px';
            } else {
                oDiv.removeChild(oHandle);
                oHandle.style.display = 'none';
            };
        });
        return oDiv;
    }

    //生成操作项
    handleCellRenderer(params, viewMode,isFixed,isEdit,customOperateList,rowOperation) {
        let rowStatus = 0;
        let operateWord=2;
        if( !params.data || ( params.node.rowGroupIndex <= this.rowGroupIndex ) || params.data&&params.data.myfooter&&params.data.myfooter == '合计' ){
            return '';
        }
        if( params.data.group||Object.is( params.data.group,'' )||Object.is( params.data.group,0 ) ){
            return '';
        }
        if(params.hasOwnProperty("data") && params["data"].hasOwnProperty("status")) {
            rowStatus = params["data"]["status"];
        }
        try {
            if(params["data"]["status"]){
                rowStatus = params["data"]["status"];
            }
        }catch (e){
            rowStatus = 0;
        }

        let str = '<div style="text-align:center;"><a class="gridView" style="color:#337ab7;">查看</a>';
        if (viewMode == 'normal'||viewMode == 'normal1'||viewMode=='source_data') {
            // if (this.isFixed || !this.isEdit ) {
            if (isFixed || !isEdit || rowStatus == 2) {
                str += ' | <span style="color: darkgrey;">编辑</span>';
                str += ' | <a style="color: darkgrey;">修改历史</a>';
            } else {
                str += ' | <a  class="gridEdit" style="color:#337ab7;">编辑</a>';
                str += ' | <a  class="gridHistory" style="color:#337ab7;">修改历史</a>';
            }
            operateWord=operateWord+6;
        }
        if(viewMode == 'batchInApprove'){
            str += ' | <a  class="gridEdit" style="color:#337ab7;">编辑</a>';
            operateWord=operateWord+2;
        }
        if(customOperateList) {
            for (let d of customOperateList) {
                str += ` | <a class="customOperate" id="${ d["id"] }" style="color:#337ab7;">${ d["name"] }</a>`;
                operateWord=operateWord+(d["name"]?d["name"].length : 0);
            }
        }
        if(rowOperation) {
            for (let ro of rowOperation) {
                if( ro.frontend_addr&&ro.frontend_addr=='export_row' ){
                    let selectedRows = JSON.stringify( [params.data._id] )
                    str += ` | <a class="rowOperation" id="${ ro["row_op_id"] }" href='/data/customize/ta_excel_export/?table_id=${ this.pageId }&selectedRows=${ selectedRows }' style="color:#337ab7;">${ ro["name"] }</a>`;
                    operateWord=operateWord+(ro["name"]?ro["name"].length : 0);
                }else {
                    str += ` | <a class="rowOperation" id="${ ro["row_op_id"] }" style="color:#337ab7;">${ ro["name"] }</a>`;
                    operateWord=operateWord+(ro["name"]?ro["name"].length : 0);
                }
            }
        }
        str += '</div>';
        // let i = 0;
        // for( let s of str ){
        //     if( s == '|' ){
        //         i++;
        //     }
        // }
        this.operateColWidth=20*operateWord+20;
        return str;
    }

    setEditableCol(col){
        let editCol = col;
        //拷贝columnDefs的值
        for(let k in col){
            editCol[k]=col[k];
        }
        //列定义可编辑部分的赋值
        let controlData = this.colControlData[editCol['colId']];
        if(controlData){
            if(controlData['is_view'] == 0 && controlData['relevance_condition']
                && Object.getOwnPropertyNames(controlData['relevance_condition']).length == 0) {
                let type = controlData['type'];
                switch (type) {
                    case 'Input':
                    case 'Textarea':
                        editCol['editable'] = true;
                        break;
                    case 'Buildin':
                    case 'Select':
                        this.setOptionsForColumn(editCol, controlData, 'options');
                        break;
                    // case 'Date':
                    //     editCol['filter']='date';
                    //     editCol['editable']=true;
                    //     // col['cellEditor']='Datepicker';
                    //     break;
                    case 'Radio':
                        this.setOptionsForColumn(editCol, controlData, 'group');
                        break;
                    default:
                        break;
                }
            }
            if(controlData['reg']){
                editCol['reg']=controlData['reg'];
            }
            if(controlData['numArea']){
                editCol['numArea']= controlData['numArea'];
            }
            // if(controlData['expression']!="") {
            //     let strings = controlData['expression'].split("@");
            //     for(let s of strings){
            //         if(s.indexOf('f')!=-1){
            //             dependency[s]= controlData['expression'];
            //         }
            //     }
            // }
            editCol['required'] = controlData['required'];
        }
        return editCol;
        // this.columnEditDefs.push(editCol);
    }

    // setEditableCol(col){
    //     let editCol = {};
    //     //拷贝columnDefs的值
    //     for(let k in col){
    //         editCol[k]=col[k];
    //     }
    //     //列定义可编辑部分的赋值
    //     let controlData = this.colControlData[editCol['colId']];
    //     if(controlData){
    //         if(controlData['is_view'] == 0
    //             && Object.getOwnPropertyNames(controlData['field_info']['edit_condition']).length == 0) {
    //             let type = controlData['type'];
    //             switch (type) {
    //                 case 'Input':
    //                 case 'Textarea':
    //                     editCol['editable'] = true;
    //                     break;
    //                 case 'Buildin':
    //                 case 'Select':
    //                     this.setOptionsForColumn(editCol, controlData, 'options');
    //                     break;
    //                 // case 'Date':
    //                 //     editCol['filter']='date';
    //                 //     editCol['editable']=true;
    //                 //     // col['cellEditor']='Datepicker';
    //                 //     break;
    //                 case 'Radio':
    //                     this.setOptionsForColumn(editCol, controlData, 'group');
    //                     break;
    //                 default:
    //                     break;
    //             }
    //         }
    //         if(controlData['reg']){
    //             editCol['reg']=controlData['reg'];
    //         }
    //         if(controlData['numArea']){
    //             editCol['numArea']= controlData['numArea'];
    //         }
    //         // if(controlData['expression']!="") {
    //         //     let strings = controlData['expression'].split("@");
    //         //     for(let s of strings){
    //         //         if(s.indexOf('f')!=-1){
    //         //             dependency[s]= controlData['expression'];
    //         //         }
    //         //     }
    //         // }
    //         editCol['required'] = controlData['required'];
    //     }
    //     this.columnEditDefs.push(editCol);
    // }

    setOptionsForColumn(editCol,controlData,optionProp:string){
        editCol['editable']=true;
        editCol['cellEditor']='select';
        let radioParams = [];
        let groups = {};
        // if(controlData['options_objs']){
        //     return;
        // }
        if(controlData[optionProp] == undefined){
            let field_content = controlData['field_content'];
            Object.getOwnPropertyNames(field_content).forEach(option=>{
                let value = option;
                let label = field_content[value];
                groups[label]=value;
                radioParams.push(label);
            });
        } else {
            controlData[optionProp].forEach(option=>{
                let value = option['value'];
                let label = option['label'];
                groups[label]=value;
                radioParams.push(label);
            })
        }

        controlData['options_objs']=groups;
        editCol['cellEditorParams']={values:radioParams};
    }

    //重置
    resetPreference (data, emitReset){
        var eHeader = document.createElement('span');
        eHeader.innerHTML = "初";
        eHeader.className = "table-init-logo";

        eHeader.addEventListener('click', ()=> {
            this.dataTableService.delPreferences( this.pageId ).then( res=>{
                if( res == 1 ){
                    this.isReset = true;
                    emitReset.emit( true );
                }
            } )
        });
        return eHeader;
    }

    //格式化参数
    formatter(num) {
        var source = String(num).split(".");//按小数点分成2部分
        if (this.accuracy == 1000) {
            source[0] = source[0].replace(new RegExp('(\\d)(?=(\\d{3})+$)', 'ig'), "$1,");//只将整数部分进行都好分割
        } else {
            source[0] = source[0].replace(new RegExp('(\\d)(?=(\\d{4})+$)', 'ig'), "$1 ");//只将整数部分进行都好分割(这个空格是万分位时用的)
        }
        return source.join(".");//再将小数部分合并进来
    }

    //footer表单columns  cellRenderer
    footerCellRenderer(params) {
        let sHtml = "",
            colDef = params.colDef;
        if (params.value == undefined) {
            params.value = "";
            return "";
        }
        if (params.value !== '') {
            try{
                if( params.value > 9007199254740992 || params.value == "计算错误" ){
                    sHtml = params.value.toString();
                }else{
                    sHtml = '<span style="float:right">' + this.formatter(Number(params.value).toFixed(colDef.real_accuracy)) + '<span/>';
                }
            }catch (err){
                sHtml = params.value;
            }
        }
        return sHtml;
    }

    addDefaultColumn(defs_filter){
        defs_filter.unshift(
            {headerName: "", field: "mySelectAll", hide: false, colId: "mySelectAll", width: this.chckboxWidth,suppressResize: true, suppressMenu: true}
        );
        defs_filter.unshift(
            {headerName: "", field: "myOperate", hide: false, colId: "myOperate", width: this.menuType ? this.chckboxWidth : 160,suppressResize: true, suppressMenu: true}
        );
        defs_filter.unshift(
            {headerName: "", field: "number", hide: false, colId: "number", width: 30, suppressSorting: true, suppressResize: true, suppressMovable: true, suppressMenu: true , cellStyle: {'padding': '0px'}}
        );
        defs_filter.unshift(
            {headerName: "", field: "myGroups", width: 200, colId: "myGroups", suppressMenu: true,suppressResize: true, hide: true}
        );
    }

    //保存搜索value值
    searchValue: any = {};
    searchOldValue: any = {};
    queryList = {};

    createSelectAndInputForFilter(oSelect,oInput,sHtml,data){
        let searchType = "keyup";
        oSelect.id = 'select';
        oSelect.className = 'filter-select';
        oSelect.innerHTML = sHtml;
        if( data.column.colDef && data.column.colDef.real_type  == 3 ){
            oInput.type = 'date';
            searchType = 'change';
        }else if( data.column.colDef && data.column.colDef.real_type  == 4 ){
            oInput.type = 'time';
            searchType = 'change';
        }else if( data.column.colDef && ( data.column.colDef.name == '创建时间' || data.column.colDef.name == '更新时间' || ( data.column.colDef && data.column.colDef.real_type  == 5 ) ) ) {
            oInput.type = 'datetime-local';
            searchType = 'change';
        }else {
            oInput.type = 'text';
        }
        oInput.className = 'filter-input';
        oInput.style.width = '80%';
        oInput.style.height = '20px';
        oInput.style.lineHeight = '20px';
        oInput.style.color = 'rgb(85,85,85)';
        oInput.style.marginTop = '6px';
        oInput.style.border = '1px solid #55A1F3';
        oInput.value = this.searchValue[data.column.colDef.field] || '';
        oInput.placeholder = data.column.colDef.is_offer_py?'提供拼音搜索':'';
        // oInput.disabled = dsearch == 0 ? true : false;
        return searchType;
    }

    // 判断搜索类型
    getMongoSearch(data) {
        switch(data){
            case "EQUALS":
                return "exact";
            case "NOT_EQUAL":
            case "NOT_EQUALS":
                return "$ne";
            case "GREATER_THAN":
                return "$gt";
            case "GREATER_THAN_OR_EQUAL":
                return "$gte";
            case "LESS_THAN":
                return "$lt";
            case "LESS_THAN_OR_EQUAL":
                return "$lte";
            case "CONTAINS":
            case "STARTS_WITH":
            default:
                return "$regex";
        }
    }

    // 获取后端搜索请求数据
    filterPostData(col_field,keyWord,searchOperate,postData) {
        this.queryList[col_field] = {
            'keyWord': keyWord,
            'operate': this.getMongoSearch(searchOperate),
            'col_field': col_field
        };
        //用于去除queryList中的空值''
        let obj = {};
        for( let key in this.queryList ){
            if( !( this.queryList[key]["keyWord"] == "" ) ){
                obj[key] = this.queryList[key];
            }
        }
        this.queryList = obj;
        postData['filter'] = [];
        for (let attr in this.queryList) {
            postData['filter'].push({
                "relation": "$and",
                "cond": {
                    "leftBracket": 0,
                    "searchBy": this.queryList[attr]['col_field'],
                    "operate": this.queryList[attr]['operate'],
                    "keyword": this.queryList[attr]['keyWord'],
                    "rightBracket": 0
                }
            });
        }
        let obj_2 = {};
        if( postData['filter'].length == 0 ){
            for( let key in postData ){
                if( key != "filter" ){
                    obj_2[key] = postData[key];
                }
            }
        }else {
            obj_2 = postData;
        }
        obj_2['is_filter'] = 1;
        return obj_2;
    }

    setFilterColumn(col){
        return {
            headerName: "搜索",
            is_offer_py: col["is_offer_py"],
            suppressMenu: true,
            field: col["field"],
            dsearch: col["dsearch"],
            isNum: this.numOrText(col["real_type"]),
            id: col['id'],
            dinput_type: col['dinput_type'],
            name: col['name']
        }
    }

    getFilterSetting(col,setting){
        if (this.numOrText(col["real_type"])) {
            setting['sHtml'] = '<option selected value="EQUALS">等于</option><option value="NOT_EQUAL">不等于</option><option value="GREATER_THAN">大于</option><option value="GREATER_THAN_OR_EQUAL">大于等于</option><option value="LESS_THAN">小于</option><option value="LESS_THAN_OR_EQUAL">小于等于</option>';
            setting['searchOperate'] = 'EQUALS';
        } else {
            setting['sHtml'] = '<option selected value="CONTAINS">包含</option><option value="EQUALS">等于</option><option value="NOT_EQUALS">不等于</option>';
            setting['searchOperate'] = 'CONTAINS';
        }
        return setting;
    }

    setBottomColumn(col){
        return {
            headerName: col["name"],
            field: col["field"],
            id: col['id'],
            suppressMenu: true,
            width: this.dataWidth,
            colId: col["field"],
            hide: false,
            dtype: col["dtype"],
            dinput_type: col["dinput_type"],
            real_type: col["real_type"],
            real_accuracy: col["real_accuracy"],
            tooltipField: col["field"],
            sortingOrder: ['desc', 'asc', null],
            enableRowGroup: false,
            cellRenderer: (params) => {
                return this.footerCellRenderer(params);
            }
        }
    }

    setColumnWidth(obj,col){
        obj["width"] = this.dataWidth;
        if (col["dinput_type"] === '5') {
            obj["width"] = 160;
        }
        if( this.colWidth && this.colWidth[col["field"]] ){
            obj["width"] = this.colWidth[col["field"]];
        }
        for( let data of this.gridState ){
            if( data.colId == col["field"] ){
                obj["width"] = data.width;
                break;
            }
        }
    }

    //对应关系选择保存用
    setcorrespondenceSelectList(isSelect,correspondenceSelectedList,_id){
        if( isSelect && correspondenceSelectedList.indexOf( _id ) == -1 ){
            correspondenceSelectedList.push( _id );
        }
        if( !isSelect && correspondenceSelectedList.indexOf( _id ) != -1 ){
            let arr2 = [];
            for( let d of correspondenceSelectedList ){
                if( d != _id ){
                    arr2.push( d );
                }
            }
            correspondenceSelectedList = arr2;
        }
        return correspondenceSelectedList;
    }

    setdeleteLists(event,deleteListRel,deleteListTemp){
        let cbox = this.domHandler.findSingle(this.el.nativeElement,'input.header_input');
        if (event.node.selected) {
            if (event.node.data.action) {
                if (deleteListTemp.indexOf(event.node.data._id) == -1) {
                    deleteListTemp.push(event.node.data._id);
                }
            } else {
                if (deleteListRel.indexOf(event.node.data._id) == -1) {
                    deleteListRel.push(event.node.data._id);
                }
            }
            let nodes = [];
            try{nodes = event.node.parent['allLeafChildren'];}catch(e){}
            for (let i = 0; i < nodes.length; i++) {
                if (!nodes[i].selected) {
                    break;
                } else if (i == nodes.length - 1) {
                    this.isSelect = false;
                    if( cbox ){
                        cbox.checked = true;
                    }
                }
            }
        } else {
            if (event.node.data.action) {
                deleteListTemp.splice(deleteListTemp.indexOf(event.node.data._id), 1);
            } else {
                deleteListRel.splice(deleteListRel.indexOf(event.node.data._id), 1);
            }
            this.isSelect = true;
            if( cbox ){
                cbox.checked = false;
            }
        }
    }

    getColumnState(event, state){
        for( let data of state ){
            if(data.colId != 'group') {
                data.hide = data.colId != 'mySelectAll' && data.colId != 'myOperate' && data.colId != 'number'
                && event['fids'].indexOf(this.colsDict[data.colId]['id']) == -1 ? true : false;
            }
        }
        let arr_1 = [];
        let arr = [];
        for( let data of event['fids'] ){
            for( let col in this.colsDict ){
                if( data == this.colsDict[col]['id'] &&　col != "_id" ){
                    arr.push( col );
                }
            }
        }
        arr_1.push( state[0] );
        arr_1.push( state[1] );
        arr_1.push( state[2] );
        for( let col of arr ){
            for( let data of state ){
                if( col == data.colId ){
                    arr_1.push( data );
                }
            }
        }
        let num = 0;
        for( let data of state ){
            for( let col of arr ){
                if( data.colId == col.colId ){
                    num++;
                }
            }
            if( num == 0 ){
                arr_1.push( data );
            }
        }
        return arr_1;
    }

    sortChangedPostData(data,postData,rowId,parentTableId,parentTempId,parentRealId,tableType,fieldId1){
        if (data) {
            postData['sortOrder']= (data.sort == "desc" ? -1 : 1);
            postData['sortField']=data.colId;
            for( let d of this.cols ){
                if( d['field'] == data.colId ){
                    postData['sort_real_type']=d['real_type'];
                }
            }
        }
        if (parentTableId) {
            var b = {};
            if (parentTempId || parentRealId) {
                b[('section_page_' + parentTableId)] = parentTempId || parentRealId;
            }
            postData['filter']=b;
            postData['parent_table_id']=parentTableId || "";
            postData['parent_temp_id']=parentTempId || "";
            postData['parent_real_id']=parentRealId || "";
            postData['rowId']=rowId || "";
            postData['tableType']=tableType || "";
            postData['fieldId']=fieldId1 || "";
            postData['childInfo']={"parent_page_id": parentTableId,"parent_row_id": parentTempId || parentRealId};
        }
    }

    //分组级数
    rowGroupIndex: number = 0;

    setGroup(state,myGroup){
        state[0]['hide'] = false;
        let j = 1;
        for( let g of myGroup ){
            for( let s of state ){
                s['pinned'] = '';
                if( g == s.colId ){
                    s.rowGroupIndex = j;
                    j++;
                }
            }
        }
        this.rowGroupIndex = j-1;
        return state;
    }

    setHideColumn(state, hide){
        this.hideColumn = this.getColumns(state, hide);
        return this.hideColumn;
    }

    getColumns(state, hide?){
        let arr = [];
        //是否全选判定
        let oSelectAll=document.getElementsByClassName('ag-column-select-checkbox')[0];
        let hideNum = 0;
        for( let s of state ){
            if( s.hide ){
                hideNum++;
            }
        }
        if( hideNum == 0 ){
            $(oSelectAll).find('.ag-checkbox-checked').removeClass('ag-hidden');
            $(oSelectAll).find('.ag-checkbox-unchecked').addClass('ag-hidden');
            $(oSelectAll).attr('data-selectAll','1');
        }else {
            $(oSelectAll).find('.ag-checkbox-checked').addClass('ag-hidden');
            $(oSelectAll).find('.ag-checkbox-unchecked').removeClass('ag-hidden');
            $(oSelectAll).attr('data-selectAll','0');
        }
        for( let col of state ){
            let flag = (hide == undefined)?true:col['hide'];
            if( col['colId'] != "mySelectAll" && col['colId'].indexOf( 'number' ) == -1 && flag ){
                arr.push( col['colId'] );
            }
        }
        return arr;
    }

    setResizedColumnWidth(state){
        this.widthList = {};
        for( let data of state ){
            this.widthList[data.colId] = data.width;
        }
    }

    getResizedColumnWidth(){
        //换成不进行宽度自适应后保存列宽
        let i = 0;
        for( let key in this.widthList ){
            i++;
        }
        if( i == 0 ){
            return;
        }else {
            this.colWidth = this.widthList;
        }
        return this.colWidth;
    }

    setMovedColumnWidth(state,isShowGroup){
        if( isShowGroup ){//分组情况下
            for( let data of state ){
                data.rowGroupIndex = null;
            }
        }
        return state;
    }

    getImageNum(num){
        if (num == 1) {
            this.imgNum = (this.imgNum == 0)? this.imgTotal - 1 : this.imgNum - 1;
        } else {
            this.imgNum = (this.imgNum == this.imgTotal - 1)? 0 : this.imgNum + 1;
        }
        return this.imgNum;
    }

    setImageNum(imgData,val){
        for (let i = 0; i < imgData.rows.length; i++) {
            if (imgData.rows[i]["file_id"] == val) {
                this.imgNum=i;
                return i;
            }
        }
        return null;
    }

    setImgData(imgData){
        for (let i = 0; i < imgData.rows.length; i++) {
            imgData.rows[i]["isSelect"] = (i == this.imgNum);
        }
    }

    sizeImageWin(){
        let oldWidth = $("#myImg").width();
        let oldHeight = $("#myImg").height();
        let fWidth = $(".imgContain").width();
        let fHeight = $(".imgContain").height();

        if (( oldWidth >= fWidth && oldHeight < fHeight ) || ( oldWidth < fWidth && oldHeight < fHeight && ( oldWidth / fWidth >= oldHeight / fHeight ) ) || ( oldWidth >= fWidth && oldHeight >= fHeight && ( oldWidth / fWidth >= oldHeight / fHeight ) )) {
            $("#ImgToShow").css({
                "width": fWidth,
                "height": "auto",
                "top": ( fHeight - fWidth / oldWidth * oldHeight ) / 2,
                "left": 0
            });
        } else {
            $("#ImgToShow").css({
                "width": "auto",
                "height": fHeight,
                "top": 0,
                "left": ( fWidth - fHeight / oldHeight * oldWidth ) / 2
            });
        }
    }

    setImgDataAndNum(res,imgData,imgSelect){
        imgData = res;
        this.imgTotal = res.rows.length;
        if(imgData){
            for( let i=0;i<imgData.rows.length;i++ ){
                imgData.rows[i]["isSelect"] = false;
            }
            if( imgData.rows[0] ){
                imgData.rows[0]["isSelect"] = true;
                imgSelect = imgData.rows[0].file_id;
            }
        }
        this.imgNum = 0;
        return {imgSelect:imgSelect,imgData:imgData};
    }

    //判断是否为对应关系
    doCorrespondence(viewMode,data){
        if(!(viewMode == 'viewFromCorrespondence' || viewMode == 'editFromCorrespondence')){
            //子表初始数据
            let sub_grid_req = {
                rowId: data.data._id,
                //childId就是子表tableId
                childId: data.colDef['field_content']['child_table']||data.colDef['field_content']['count_table'],
                parentRealId: data["data"]["_id"],
                fieldId:data.colDef.id,
                fieldContent:JSON.stringify(data.colDef['field_content'])
            };
            //给子表弹出框的header用的数据
            let childName = {};
            childName['parentTableName'] = data['colDef']['tableName'];
            childName['parentFieldName'] = data['colDef']['headerName'];
            childName['parentStandName'] = '';
            childName['childTableName'] = data.colDef['field_content']['child_table_name'];
            if(this.childTable(data.colDef.dinput_type)){
                for(let i = 0;i<this.cols.length;i++){
                    if(this.cols[i]['is_stand'] == 1){
                        let field = this.cols[i]['field'];
                        if(field != '_id'){
                            childName['parentStandName'] = data['data'][field]||'';
                        }
                    }
                }
                sub_grid_req['childName']=data.colDef['tableName'] + '->' + data.colDef['field_content']['child_table_name'];
                sub_grid_req['parentTableName']=data.tableName;
                sub_grid_req['tableType']='child';
                sub_grid_req['tempId']=data['data']['temp_id'];
                this.dataGridMissionService.missionSubdataGridPage(sub_grid_req);
            } else if(this.countTable(data.colDef.dinput_type,data.colDef.real_type)){
                var showName;
                try {
                    showName =JSON.stringify(childName) ;
                }catch (err){
                    showName = data.colDef['field_content']['child_table_name'];
                }
                if(data.colDef['field_content']['child_table_name']){
                    sub_grid_req['childName']=data.colDef['tableName'] + '->' + data.colDef['field_content']['child_table_name'];
                }else {
                    sub_grid_req['childName']=data.colDef['tableName'] + '->' + data.colDef['field_content']['count_table_name'];
                }
                sub_grid_req['tableType']='count';
                this.dataGridMissionService.missionSubdataGridPage(sub_grid_req);
            }
        }
    }

    showFilter(isShowFilter){
        let header = this.el.nativeElement.querySelector(".ag-header");
        let body = this.el.nativeElement.querySelector(".ag-body");
        let viewport = this.el.nativeElement.querySelector(".ag-body-viewport");
        this.renderer.setElementStyle(body, "margin-top", isShowFilter? "5px":'-20px');
        this.renderer.setElementStyle(viewport, "height", isShowFilter? "100%":'calc( 100% + 20px )');
    }

    showGroupAndCustom(hideCol,isGroup,gridOption_top){
        let contain =  this.el.nativeElement.querySelectorAll(".ag-tool-panel");
        let hideFilter = this.el.nativeElement.querySelectorAll(".hideFilter");
        this.createHidefilter(gridOption_top);
        let group =  this.el.nativeElement.querySelectorAll(".ag-column-drop");
        let selectBox =  this.el.nativeElement.querySelectorAll(".ag-column-select-checkbox");
        this.renderer.setElementStyle( contain[0], "overflow", "hidden" );
        this.renderer.setElementStyle( contain[0], "border", "1px solid #71abf3" );
        this.renderer.setElementStyle( contain[0], "position",isGroup ? '' : "absolute" );
        this.renderer.setElementStyle( hideCol[0], "overflow-y", "scroll" );
        this.renderer.setElementStyle( hideCol[0], "height", isGroup ? "50%" : "100%" );
        this.renderer.setElementStyle( hideCol[0], "padding-top", "10px" );
        this.renderer.setElementStyle( group[2], "height", "25%" );
        this.renderer.setElementStyle( group[3], "height", "25%" );
        this.renderer.setElementStyle( group[2], "width", "100%" );
        this.renderer.setElementStyle( group[3], "width", "100%" );
        this.renderer.setElementStyle( group[2], "overflow-y", "scroll" );
        this.renderer.setElementStyle( group[3], "overflow-y", "scroll" );
        this.renderer.setElementStyle( hideCol[0], "display", "block" );
        this.renderer.setElementStyle( group[2], "display", "block" );
        this.renderer.setElementStyle( group[2], "display", isGroup ? "block" : "none" );
        // for( let i=0;i<selectBox.length;i++ ){
        //     // this.renderer.setElementStyle( selectBox[i], "display", isGroup ? "none" : "inline" );
        // }
    }

    //创建定制列搜索
    createHidefilter (gridOptions_top){
        let hideCol =  this.el.nativeElement.querySelectorAll(".ag-column-select-panel");
        let hideFilter = this.el.nativeElement.querySelectorAll(".hideFilter");
        if( hideFilter.length == 0 ){
            if(!hideCol[0]){
                return;
            }
            let divs = hideCol[0].querySelectorAll("div");
            let oInput = document.createElement('input');
            let oSelectAll=document.getElementsByClassName('ag-column-select-checkbox')[0].cloneNode(true);
            let isSelectAll=1;
            let This=this;
            for( let div of divs ){
                let $checkBox = $(div).find('.ag-column-select-checkbox');
                isSelectAll=$checkBox.find('.ag-checkbox-checked').hasClass('ag-hidden')?0:1;
                if(isSelectAll == 0){
                    $(oSelectAll).find('.ag-checkbox-checked').addClass('ag-hidden');
                    $(oSelectAll).find('.ag-checkbox-unchecked').removeClass('ag-hidden');
                    break;
                }
            }
            $(oSelectAll).attr('data-selectAll',isSelectAll);
            $(oSelectAll).css({'margin-top':'2px','margin-left':'17px','position':'absolute'});
            oSelectAll.addEventListener('click',()=>{
                if($(oSelectAll).attr('data-selectAll') == '1') {
                    gridOptions_top.columnApi.setColumnsVisible(gridOptions_top.columnApi.getAllColumns(),false);
                    $(oSelectAll).find('.ag-checkbox-checked').addClass('ag-hidden');
                    $(oSelectAll).find('.ag-checkbox-unchecked').removeClass('ag-hidden');
                    $(oSelectAll).attr('data-selectAll','0');
                }else{
                    gridOptions_top.columnApi.setColumnsVisible(gridOptions_top.columnApi.getAllColumns(),true);
                    $(oSelectAll).find('.ag-checkbox-checked').removeClass('ag-hidden');
                    $(oSelectAll).find('.ag-checkbox-unchecked').addClass('ag-hidden');
                    $(oSelectAll).attr('data-selectAll','1');
                }
            });
            hideCol[0].insertBefore(oSelectAll,divs[0]);
            oInput.value = "";
            oInput.className = "hideFilter";
            oInput.placeholder = "定制列搜索";
            oInput.style.marginLeft = '37px';
            oInput.id = "filterHideCol";
            hideCol[0].insertBefore( oInput ,divs[0] );
            $(hideCol[0]).css('position','relative');
            let filterHideCol =  this.el.nativeElement.querySelector("#filterHideCol");
            filterHideCol.addEventListener( 'keyup',()=> {
                for( let div of divs ){
                    this.renderer.setElementStyle( div, "display", ( div.innerText.indexOf( oInput.value ) == -1 && oInput.value != "" ) ? "none" : "block" );
                }
            } )
        }
    }

    reorderFooterConbineWithGrid(state,columnDefs){
        let fieldToColName = {};
        for( let col of columnDefs ){
            fieldToColName[col.field] = col.headerName;
        }
        let selectCols = this.el.nativeElement.querySelectorAll('.ag-column-select-column');
        let hidePanel =  this.el.nativeElement.querySelectorAll(".ag-column-select-panel");
        for( let s of state ){
            for( let c of selectCols ){
                if( c.children[2].innerHTML == fieldToColName[s.colId] ){
                    $(c).remove();
                    $(hidePanel[0]).append( c );
                }
            }
        }
        return state;
    }

    requestBuildinSource(data){
        let colDef = data.colDef;
        let json = {
            'source_table_id': colDef.source_table_id,
            'base_buildin_dfield': colDef.base_buildin_dfield,
            'rowId': data.data._id,
            'tableType': 'source_data',
            'tableIdForBuildin': this.pageId,
            'source_field_dfield': colDef.source_field_dfield,
            'winName': data['colDef']['tableName'] + '->' + data['colDef']['source_table_name']
        };
        this.dataGridMissionService.missionSubdataGridPage( json );
    }

    requestCorrespondenceSource(data,correspondenceView){
        let json = {
            form_id:'',
            table_id:this.pageId,
            is_view:1,
            parent_table_id:'',
            parent_real_id:'',
            parent_temp_id:'',
            real_id:data['data']['_id']
        };
        this.dfService.getDynamicFormData2(json).then(res=>{
            if(res.success == '1'){
                let sub_grid_req = {
                    childId:data['colDef']['field_content']['correspondence_table_id'],
                    rows:9999,
                    parent_table_id:this.pageId,
                    parentRealId:data['data']['_id'],
                    tempId:res.data.temp_id.value
                };
                let childName = {};
                childName['parentTableName'] = data['colDef']['tableName'];
                childName['parentFieldName'] = data['colDef']['headerName'];
                childName['parentStandName'] = '';
                childName['childTableName'] = data.colDef['field_content']['table_name'];
                sub_grid_req['childName']=data.colDef['tableName'] + '->' + data.colDef['source_table_name'];
                sub_grid_req['parentTableName']=data.tableName;
                this.dataGridMissionService.missionSubdataGridPage(sub_grid_req);
                correspondenceView.emit(true);
            }
        })
    }

    calcColumnStateOnColOrder(moveColIndex,toDivIndex,state){
        let arr = [];
        let obj = state[moveColIndex];
        for( let i=0;i<state.length;i++ ){
            if( i!=moveColIndex ){
                arr.push( state[i] );
            }
        }
        arr.splice( toDivIndex,0,obj );
        return arr;
    }

    calcColumnDefsAndfixCols(columnDefs,obj,another,colName,isL){
        let colField = this.colNameToField[colName];
        let index,isSuppressMovable;
        if( obj.className.indexOf( 'fixSelect' ) == -1 ){
            isSuppressMovable = true;
            index = isL ? 'r' : 'l';
            obj.className+= isL ? / fixSelect-r/ : / fixSelect-l/;
           if( another.className.indexOf( 'fixSelect' ) != -1 ){
                another.className = another.className.replace(isL ? ' fixSelect-l' : ' fixSelect-r', "");
            }
            this.fixCols[isL ? 'l' : 'r' ].push( colField );
        }else {
            isSuppressMovable = false;
            index = isL ? 'l' : 'r';
            obj.className = obj.className.replace(isL ? / fixSelect-l/ : / fixSelect-r/, "");
        }
        for( let col of columnDefs ){
            if( col.field == colField ){
                col['suppressMovable'] = isSuppressMovable;
            }
        }
        for( let i=0;i<this.fixCols[index].length;i++ ){
            if( this.fixCols[index][i] == colField ){
                this.fixCols[index].splice( i,1 );
            }
        }

    }

    calcFixArr(state){
        let stateArr = [{colId:'number',pinned:this.fixCols.l.length==0?'':'left',hide:false },
            {colId:'mySelectAll',pinned:this.fixCols.l.length==0?'':'left',hide:false}];
        for( let d of this.fixCols.l ){
            stateArr.push( { colId:d,pinned:'left',hide:false } )
        }
        let arr = ['number','myOperate','mySelectAll'];
        arr = arr.concat(this.fixCols.r);
        arr = arr.concat(this.fixCols.l);
        for( let d of state ){
            if( arr.indexOf(d.colId) == -1 ){
                if(this.hideColumn.indexOf( d.colId ) != -1){
                    stateArr.push( { colId:d.colId,pinned:'',hide:true } )
                }else{
                    stateArr.push( { colId:d.colId,pinned:'',hide:false } )
                }
            }
        }
        for( let d of this.fixCols.r ){
            stateArr.push( { colId:d,pinned:'right',hide:false } )
        }
        stateArr.push( {colId:'myOperate',pinned: this.fixCols.r.length == 0 ? null : 'right' ,hide:false} );
        return stateArr;
    }

    getAllColumnIds(columnDefs){
        let allColumnIds = [];
        columnDefs.forEach(function (columnDef) {
            if (columnDef.field && columnDef.dinput_type != 23 && columnDef.field != 'mySelectAll' && columnDef.field != 'myOperate' && columnDef.field != 'number' ) {
                allColumnIds.push(columnDef.field);
            }
        });
        return allColumnIds;
    }

    getOldColumnWidth(arr,colStates){
        for( let data of colStates ){
            for( let col of arr ){
                if( data['colId'] == col['colId'] ){
                    col['width'] = data['width'];
                }
            }
        }
        return arr;
    }

    getCorrespondenceRows(rowData,correspondenceSelectedList){
        let arrCorr=[];
        if(!rowData){
            return;
        }
        for(let row of rowData ){
            if(correspondenceSelectedList.indexOf(row['_id']) != -1){
                arrCorr.push(row);
            }
        }
        return arrCorr;
    }

    //行内数据改变时
    onCellValueChanged($event,changeFieldList,newValueChangeList,oldValueChangeList) {
        let newChangeValue = {};
        let oldChangeValue = {};
        let changeField = $event.colDef.field;
        newChangeValue[changeField] = $event.newValue;
        oldChangeValue[changeField] = $event.oldValue;
        if (newChangeValue[changeField] == oldChangeValue[changeField]) {
            return;
        }
        for (let i = 0, len = changeFieldList.length; i < len; i++) {
            if (changeFieldList[i]['field'] == changeField) {
                newValueChangeList.push(newChangeValue);
                oldValueChangeList.push(oldChangeValue);
                return;
            }
        }
        newValueChangeList.push(newChangeValue);
        oldValueChangeList.push(oldChangeValue);
        changeFieldList.push({headerName: $event.colDef.headerName, field: changeField, width: 120, pinned: true});
    }

    setBtnDisplay(viewMode,isFixed,isEdit,rowStatus, btn_display?:string, extra_condition?:boolean){
        extra_condition = (extra_condition==undefined)?true:extra_condition;
        if(btn_display == undefined || btn_display == ""){
            //如果是正常状态下，显示“转到编辑模式”
            return ((viewMode == 'normal'||viewMode == 'normal1'||viewMode=='source_data') && !isFixed && isEdit && extra_condition && rowStatus != 2)? 'bianji': 'none';
        } else {
            return btn_display;
        }
    }

    getHiddenColumnIds(){
        this.hideColumn = [];
        for( let data of this.gridState ){
            if( data.hide ){
                this.hideColumn.push( data.colId );
            }
        }
    }

    //清空搜索条件
    clearFilter (){
        let inputs = this.el.nativeElement.querySelectorAll( ".filter-input" );
        for( let data of inputs ){
            data.value = "";
        }
        this.queryList = {};
    }

    //判断数字类型
    numOrText(data) {
        return (data == 10 || data == 11 || data == 16);
    }

    //判断子表类型
    childTable(data) {
        return data == 13;
    }

    //判断统计类型
    countTable(data,data2?) {
        //data2高级跳转字段根据real_type判断
        return data == 15||data2 == '35'||data == '24';
    }

    //判断整数小数
    intOrFloat(data) {
        return (data == 11); //11是整数 10是小数
    }

    //操作添加空的div
    addDiv() {
        var eHeader = document.createElement('div');
        return eHeader;
    }

    // sheet页显示与否调整尺寸
    isSheet() {
        let dataHeight = this.domHandler.findSingle(this.el.nativeElement, 'div.myData');
        dataHeight.style.height = 'calc( 100% - 15px )'
    }

    //判断Object是否相等
    checkObejctNotEqual(obj1,obj2){
        let o1=Object.assign({},obj1);
        let o2=Object.assign({},obj2);
        if(Object.prototype.toString.call(o1)!='[object Object]'){
            if(o1 != o2){
                return true;
            }else{
                return false;
            }
        };
        if(JSON.stringify(o1) == JSON.stringify(o2)){
            return false;
        }else{

            return true;
        }
    }

    //比对当前值与初始值的差别
    getChangedRows(rowData){
        let changedRows = {};
        rowData.forEach((row,index)=>{
            let real_id = row['_id'];
            let originRow = this.originRowData[real_id];
            let changed = {};
            let data = {};
            for (let k in row) {
                if (this.checkObejctNotEqual(row[k],originRow[k])) {
                    //buildin字段做转化
                    if(this.colControlData[k]['type'] == 'Buildin'
                        || this.colControlData[k]['type'] == 'Radio'
                        || this.colControlData[k]['type'] == 'Select'){
                        data[k] = this.colControlData[k]['options_objs'][row[k]];
                    } else if(Array.isArray(row[k])){
                        if(row[k].length == 0 && originRow[k].length == 0){
                            continue;
                        }
                        for(let i = 0,length = row[k].length;i < length; i++){
                            if(row[k][i]!=originRow[k][i]){
                                data[k] = row[k];
                                break;
                            }
                        }
                    }else {
                        data[k] = row[k];
                    }
                }
            }
            if(Object.getOwnPropertyNames(data).length!= 0) {
                data['real_id'] = real_id;
                data['table_id'] = this.pageId;
                changed['data'] = data;
                changed['cache_old'] = this.originRowData[real_id];
                changed['cache_new'] = row;
                changed['table_id'] = this.pageId;
                changed['focus_users'] = [];
                changedRows[real_id] = changed;
            }
        });
        return changedRows;
    }

    abjustTargetRow(targetRow,ids:{}){
        let pRealId = ids['parent_real_id'];
        let pTableId = ids['parent_table_id'];
        let pTempId = ids['parent_temp_id'];
        let tempId = ids['temp_id'];
        this.fillIdsInObj(targetRow['data'],pRealId,pTableId,pTempId,tempId);
        this.fillIdsInObj(targetRow['cache_new'],pRealId,pTableId,pTempId,tempId);
        this.fillIdsInObj(targetRow['cache_old'],pRealId,pTableId,pTempId,tempId);
        this.fillIdsInObj(targetRow,pRealId,pTableId,pTempId);
        return targetRow;
    }

    fillIdsInObj(obj,pRealId,pTableId,pTempId,temp_id?){
        if(!obj){
            return;
        }
        if(temp_id){
            obj['temp_id']=temp_id||'';
        }
        obj['parent_real_id']=pRealId||'';
        obj['parent_table_id']=pTableId||'';
        obj['parent_temp_id']=pTempId||'';
    }

    getDeployList(rowData, realId,field){
        let deploylist=[];
        for(let row of rowData){
            if(row['_id']==realId){
                deploylist=row[field];
            }
        }
        return deploylist;
    }

    setEditIcon(flag:string){
        let icon_url;
        switch(flag){
            case 'editable':
                icon_url = require("../component/data-table-page/data-table-page-data/icon_edit_cell.ico");
                break;
            case 'save':
                icon_url = require("../component/data-table-page/data-table-page-data/icon_save_edit.ico");
                break;
            case 'unEditable':
            default:
                icon_url = require("../component/data-table-page/data-table-page-data/icon_grey_edit_cell.ico");
        }
        if(this.el.nativeElement.querySelector("#edit_cell")) {
             this.el.nativeElement.querySelector("#edit_cell").style.background = 'url(' + icon_url + ') no-repeat';
        }
    }

    setRemindColor(res){
        try {
            this.remindColor = res;
        }
        catch (err) {
            this.remindColor = {};
        }
    }

    setPreferences(res){
        if (res['colWidth']) {
            this.colWidth = res['colWidth'].colWidth;
            if( typeof ( this.colWidth ) == 'string'){
                this.colWidth = JSON.parse( res['colWidth'].colWidth );
            }
        }
        if (res['pageSize'] && res['pageSize'].pageSize) {
            this.rows = res['pageSize'].pageSize;
        }
        if (res['ignoreFields']) {
            this.hideColumn = JSON.parse(res['ignoreFields']['ignoreFields']);
        }
        else{
            this.hideColumn = ['f1','f2','f3','f4']
            let json = {
                action:'ignoreFields',
                table_id:this.pageId,
                ignoreFields:JSON.stringify( this.hideColumn ),
            }
            this.dataTableService.savePreference( json );
        }
        if (res['fieldsOrder']) {
            this.orderFields = JSON.parse(res['fieldsOrder']['fieldsOrder']);
        }
        if (res['pinned'] && res['pinned']['pinned']) {
            this.fixCols = JSON.parse(res['pinned']['pinned']);
        }
    }

    addOptions(options){
        let dfield = options['dfield'];
        let optionArray = [];
        let optionLableValueArray = {};
        options['options'].forEach(option=>{
            optionArray.push(option['label']);
            optionLableValueArray[option['label']]=option['value'];
        })
        for(let i = 0,length = this.columnEditDefs.length;i < length; i++){
            let obj = this.columnEditDefs[i];
            if(obj['field']==dfield) {
                obj['cellEditorParams']['values'] = optionArray;
                break;
            }
        }
        this.colControlData[dfield]['options_objs']=optionLableValueArray;
    }
}
