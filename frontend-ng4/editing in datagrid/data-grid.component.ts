import {
    Component, Input, ElementRef, Renderer, Output, EventEmitter, ViewChild, ChangeDetectorRef, ViewEncapsulation,
    OnDestroy
} from '@angular/core';
import {GridOptions} from "ag-grid/main";
// import 'ag-grid-enterprise/main';

import {DataTableService} from '../../service/dataTable.service';
import {RemindService} from '../../service/remind.service';
import {DataGridMissionService} from '../../service/data-grid-mission.service';
import {BaseService} from "../../service/base.service";
import {UserService} from "../../service/user.service";
// import {TimeControlComponent} from "../time-control/time-control.component";
import { DynamicFormService } from "../../service/dynamic-form.service"
import { WorkflowService } from '../../service/workflow.service';
import { Observable, Subscription } from "rxjs/Rx";
import { dataRowOperateComponent } from '../data-row-operate/data-row-operate.component';
import { GlobalService } from '../../service/global.service';
import {DomHandler} from '../vendor/domhandler';
import {FunctionLib} from '../../service/function-lib.service';
import {replacingIcons} from './data-grid.models';
import { DataGridControlService } from "../../service/data-grid-control.service";
import {document} from "@angular/platform-browser/src/facade/browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DataModule } from "../../modules/data.module";
import { HistoryModule } from "../../modules/history-modify.module";
import {SafeResourceUrl} from "@angular/platform-browser";
import {CustomColumnsComponent} from "../data-table-toolbar/custom-columns/custom-columns.component";
import {ContractEditorService} from "../../service/contract-editor.service";
// import indexOf = require("core-js/fn/array/index-of");

@Component({

    selector: 'my-datagrid',
    templateUrl: './data-grid.component.html',
    styleUrls: ['./data-grid.component.css'],
    providers: [
        BaseService,
        RemindService,
        DomHandler,
        FunctionLib,
        DynamicFormService,
        ContractEditorService
    ],
    encapsulation: ViewEncapsulation.None
})
export class datagridComponent implements OnDestroy {
    //组件名，用于console
    selector = 'my-datagrid';
    isShowEnclosure:boolean = false;
    fieldids = [];
    file_dinput_type:any = '';
    @Input() keyword:string;
    @Input() fieldContent:any;
    @Input() fieldId1:any;
    @Input() queryParams:any = {};
    //子表参数
    @Input() subPageId: number;
    @Input() rowId:any;
    @Input() tableType:any = '';
    @Input() subRowId: string;
    @Input() subChildId: number;
    @Input() subTableType: string;
    //子表参数wyn
    @Input() parentTableId: string;
    @Input() parentRealId: string;
    @Input() parentTempId: string;
    @Input() parentRecordId: string;
    //批量工作流
    @Input() isBatch: string | number;
    //批量工作流要显示的ids
    @Input() tempIdsForBatchInApprove = [];
    //是否来自内联子表
    @Input() songridFromInline: string;
    //namespace用来判别是否为外部表
    @Input() namespace: string;

    //是否为显示对应关系已选择数据
    @Input() showCorrespondenceSelect: boolean = false;

    //recordId

    @Input() recordId:string;
    @Input() cannotopenform:string;
    //对应关系回显的ids
    correspondenceSelectedList = [];
    //对应关系增加的ids
    correspondenceAddList = [];
    //对应关系减少的ids
    correspondenceRemoveList = [];
    // table的id
    @Input() pageId: number;

    //请求参数
    postData:{};
    //数据总和
    total:number;
    //选中rows
    deleteListRel:any[] = [];
    deleteListTemp:any[] = [];
    isFilterShow: boolean = true;
    //datagrid数据返回状态
    bodyDataStatus: boolean = false;
    headerDataStatus : boolean = false;
    //定义三个datagrid对象
    gridOptions_top:GridOptions;
    // gridOptions_bottom:GridOptions;
    // gridOptions_filter:GridOptions;
    gridOptions_change: GridOptions;
    //表单数据
    rowData:{}[];
    //footer表单数据
    rowData_footer:any = [];
    //表单查询数据
    rowData_filter:{}[] = [];
    //表单头部数据
    @Input() columnDefs:any[] = [];
    //footer表单头部数据
    columnDefs_bottom:any[] = [];
    columnDefs_filter:any[];
    onChanges_time:number = 0;
    //filter显示隐藏
    isShowFilter:boolean = false;
    // Group操作
    myGroup: any[] = [];
    isShowGroup:boolean = false;
    //col状态
    autoResizeWidth = true;
    //点击编辑或者查看的real_id，其实就是row_id
    realId: string| number;
    //点击编辑或者查看的real_id，其实就是row_id
    action: string | number;
    //判断是查看还是编辑
    isView: number|string = 0;
    //什么模式进来的
    @Input() viewMode: string = '0';
    //弹窗是否显示
    display: boolean;
    //打开窗口后，显示哪个按钮
    btnDisplay: string;
    //所有改变行 内容
    changeWindow: boolean = false;
    changeFieldList: any[] = [];
    newValueChangeList: any[] = [];
    oldValueChangeList: any[] = [];
    //当前表单状态（ 查看 编辑 ）
    datagridStatus = true;
    //编辑状态下弹窗
    datagridEditWin = false;
    //编辑状态下
    //rowId: string='';
    @Input() fieldId: string;
    currentValue: string;
    editField_f:string;
    editRowIndex: number;

    //自定义操作
    @Input() customOperateList = [];
    //自定义操作的customFormId
    customFormId;
    //自定义操作的customRowId
    customRowId;
    //自定义操作的customId
    customId;
    //自定义操作的customFlowId
    customFlowId;
    //自定义操作的customTableId
    customTableId;
    //自定义操作的窗口显示
    customDisplay: boolean = false;
    rowDisplay:boolean=false;
    //显示流程节点
    flow_node : any = [];
    //生命周期
    lifecycle:boolean=false;
    //自定义行级操作
    @Input() rowOperation = [];

    // 弹窗参数
    imgType:string;
    alertVisible:boolean;
    alertConfirm:string;
    //2是在工作流中
    rowStatus: number = 0;

    //被选中数组
    @Output() selectedArr = new EventEmitter<any>();

    //loading动画
    @Output() isLoading = new EventEmitter<boolean>();

    //传递宽度自适应
    @Output() emitAutoWidth = new EventEmitter<boolean>();

    //传递关掉分组
    @Output() emitCloseGroup = new EventEmitter<boolean>();

    //向父级传递搜索数据
    @Output() emitQueryList = new EventEmitter<any>();

    //向父级传递表类型
    @Output() emitNameSpace = new EventEmitter<any>();

    //刷新完成
    @Output() isRefreshFinish: EventEmitter<any> = new EventEmitter();

    //如果加载的是常用查询向父级传递数据
    @Output() emitCommonQuery: EventEmitter<any> = new EventEmitter();

    //初始化偏好
    @Output() emitReset: EventEmitter<any> = new EventEmitter();

    //快速搜索参数
    @Output() emitFastSearch: EventEmitter<any> = new EventEmitter();
    buttonType:string;
    missionQuerySourceSub: Subscription;
    idsImportedSub: Subscription;
    userInfoSub: Subscription;
    dataSearchSub: Subscription;

    openAlert(param, img_type,buttonType?: string){
        this.alertVisible=true;
        this.imgType= img_type;
        this.alertConfirm=param;
        this.buttonType=buttonType;
    }

    //是否固化
    isFixed: boolean = false;
    //查看编辑弹窗
    oHandle:any;
    //查看编辑弹窗的全局监听
    documentClickListener:any;

    //增删改查权限设置
    @Output() myPerm = new EventEmitter<any>();

    //权限合集
    permission: any = {add: 1, calendar: 1, complex_search: 1, custom_field: 1, custom_width: 1, delete: 1, download: 1, edit: 1, group: 1, in_work: 1, search: 1, upload: 1, view: 1 ,setting: 1,cell_edit:1,new_window:1};
    // //样式管理
    // stylelocal:any;
    // stylenow:any;
    //字体设置
    gridFont:string='12px';
    //弹窗位置
    subWindowTop: number = 0;
    subWindowLeft: number = 0;

    //个人用户编辑表中的数据时,会发起对应的工作流,不让用户编辑
    status: any = '';
    //报错信息
    gridErr: string = "";
    //查看子组件
    @ViewChild( dataRowOperateComponent)
    dataRowOperateComponent: dataRowOperateComponent;

    //是否有sheet分页
    @Input() isShowSheet: boolean = true;

    //修改历史弹窗参数
    isShowGridHistory: boolean = false;

    //原始表table_id
    @Input() source_table_id: string = '';
    //原始表字段id
    @Input() base_buildin_dfield: string = '';
    @Input() tableIdForBuildin: string = '';
    @Input() source_field_dfield: string = '';

    //需要注入到全景图组件中的参数组
    panorama:boolean=false;
    p1status:any={};
    p2status:any={};
    specialstatus:any={};
    p3status: any={};
    p4status: any={};
    p5status: any={};
    myDataAll:any;
    panoramaDataList:any=[];
    panoramaDataList2:any=[];
    searchKey:string='';
    wf_table_id3:string='';
    formId3:string='';
    isView3:number=1;
    recordId3:string='';
    flowId3:string='';
    display5:boolean=false;

    goSearchConSub;
    dataInvaliSub;
    addOptionSub;

    @Input() isCreateFalseTable:boolean=false;

    @Input() formValue:any;

    //合同预览相关参数
    isShowContract:boolean=false;
    isShowContractLoading:boolean=true;
    showContractHTML:any;

    constructor(
        private dataTableService:DataTableService,
        private dataGridMissionService: DataGridMissionService,
        private dgcService: DataGridControlService,
        private el: ElementRef,
        private renderer: Renderer,
        private remindService: RemindService,
        private wfService: WorkflowService,
        private globalService: GlobalService,
        private domHandler:DomHandler,
        private cd: ChangeDetectorRef,
        private dfService:DynamicFormService,
        private userService:UserService,
        private functionLib:FunctionLib,
        private ceService:ContractEditorService
    ) {
        this.gridOptions_top = <GridOptions>{};
        let _this_ = this;
        this.gridOptions_top = <GridOptions>{
            // getNodeChildDetails: this.getNodeChildDetails,
            animateRows: true,
            floatingFilter:true,
            enableFilter: true,
            slaveGrids: [],
            getRowStyle(param) {
                // 设置对应关系的背景色
                return _this_.dgcService.setRowStyle(param,_this_.correspondenceAddList,_this_.correspondenceRemoveList);
            },
            icons: replacingIcons,
            onGridReady() {
                //解决第一次加载界面如果没有数据，暂无数据图片不显示的问题。
                _this_.gridOptions_top.api.setRowData( _this_.rowData );
                //对应关系的回显勾选
                _this_.gridOptions_top.api.forEachNode( function (node) {
                    if( !node["data"] ){//处理在group中，报错
                        return;
                    }
                    let id = node["data"]["_id"];
                    if(_this_.correspondenceSelectedList.indexOf(id) != -1){
                        node.setSelected(true);
                    }
                });
                //只显示对应关系数据
                if( _this_.showCorrespondenceSelect ){
                    setTimeout( ()=>{
                        _this_.onlyShowCorrespondenceData();
                    },700 )
                }
            },
            //原表编辑数据校验
            onCellValueChanged(event){
                let error = _this_.dgcService.verifyCellData(event);
                if(error){
                    _this_.openAlert(error,'error');
                }
            }
        };
        this.gridOptions_change = <GridOptions>{};

        //--------------------------------Yunxuan Yan--------------------------------
        // This is used to combine two responses from server with RsJx
        Observable.zip(
            this.dataTableService.columnListSub,
            this.dfService.staticFormDataSub,
            (columnList,staticForm)=>{
                //表头提醒颜色赋值
                this.dgcService.fieldColor = columnList['field_color'] || {};
                if(staticForm){
                    this.dgcService.getColumnControlData(staticForm,
                        [FormsModule, ReactiveFormsModule, DataModule, HistoryModule]);
                }
                this.dgcService.cols = columnList.rows;
                //权限设置（向父级data-table-page-data传递）
                if( columnList["permission"] ){
                    this.permission = columnList["permission"];
                    this.myPerm.emit(columnList["permission"]);
                }
                //快速搜索的数据
                let fastSearch = this.dgcService.setValuesFromCols();
                if( fastSearch.length != 0 ){
                    this.emitFastSearch.emit( fastSearch );
                }

                this.namespace = columnList.namespace;
                this.emitNameSpace.emit( columnList.namespace );
                this.dgcService.menuType = columnList.menu_type;
                this.createColumnDefs();
                this.headerDataStatus = true;
                this.sortWay();
                //定制列数据
                this.columns = [
                    {name:'序号',field:'number'},
                    {name:'选择',field:'mySelectAll'},
                    {name:'操作',field:'myOperate'}
                ];
                for( let c of this.dgcService.cols ){
                    if( c['name'] != 'ID' ){
                        this.columns.push( {name:c['name'],field:c['field'],real_type:c['real_type']} )
                    }
                }
                this.cd.markForCheck();
            }).subscribe(
            function (x) {},
            function (err) {console.error(err.stack)},
            function () {console.log('Completed',this.selector);}
        );
        // End--------------------------------Yunxuan Yan--------------------------------

        this.addOptionSub = this.dataTableService.addOptions.subscribe(res =>{
            if(!res){
                return;
            }
            this.dgcService.addOptions(res);
        });

        //订阅高级查询结果
        this.missionQuerySourceSub = this.dataGridMissionService.missionQueryDataSource.subscribe(res => {
            if( !res ){
                return;
            }
            if(res.total == -1){
                this.rowData = [];
                this.total = 0;
            }else {
                this.rowData = res.rows;
                this.total = res.total;
            }
            this.cd.markForCheck();
        });
        //订阅批量工作流中，导入成功后返回的id集合
        this.idsImportedSub = this.dataGridMissionService.idsAfterImportSuccess.subscribe(res => {
            if(res !== null){
                //如果是批量工作流的话，赋值id数据，并刷新datagrid
                if(this.isBatch == '1'){
                    this.dgcService.idsOfBatchList = res;
                }
                //此处吧getDataGridData注释了，导入成功之后在data-table-page-data里面刷新
                // this.getDataGridData();
                this.cd.markForCheck();
            }
        });
        //订阅数据失效推送
        this.dataInvaliSub = this.globalService.dataInvali$.subscribe(res=> {
            if(+res==this.pageId){
                setTimeout(()=>{
                    this.alertVisible = false;
                }, 500 );
            }
        });
        this.goSearchConSub = this.globalService.goSearchCon.subscribe(res=>{
            if(res){
                this.dgcService.globalFilter=res[0];
            }
        });
    }

    //监听tab切换
    ngOnChanges(changes:{}) {
        //是否有sheet分页改变数据div高
        if( changes['isShowSheet']&&!changes['isShowSheet']['currentValue'] ){
            this.dgcService.isSheet();
        }
    }

    ngOnInit (){
        // this.isFilterShow = false;
        if(UserService.sysConfig){
            this.getPreferences();
        } else {
            this.userInfoSub = this.userService.UserAllInfo.subscribe(res=>{
                this.getPreferences();
            })
        }
        if(!this.isCreateFalseTable){
            this.pageId = this.dgcService.judgeType(this.subRowId,this.subChildId,this.subTableType,this.viewMode,this.pageId)||this.pageId;
        }
        //请求偏好（隐藏字段List，group，字段排序）
        // this.rowData_filter = [{filter: "搜索"}];
        this.dgcService.isSelect = true;

        this.dataSearchSub = this.dataTableService.dataSearch.subscribe( res=>{
            if( res ){
                let obj_2 = this.dgcService.filterPostData(res.col_field,res.keyWord,res.searchOperate,this.postData);
                this.dgcService.searchDataState = obj_2['filter'] || null;
                this.justSearch( obj_2 );
            }
        } )
    }

    //请求偏好设置
    getPreferences() {
        this.dataTableService.getPreferences({
            'actions': ['ignoreFields', 'group', 'fieldsOrder', 'pageSize','colWidth','pinned'],
            'table_id': this.pageId
        }).then(res => {
            this.dgcService.setPreferences(res);
            this.fixCols = this.dgcService.fixCols;
            this.myGroup = (res['group'] != undefined)?JSON.parse(res['group'].group):[];
            //请求提醒数据
            this.remindService.getRemindInfo(this.pageId).then(res => {
                this.dgcService.setRemindColor(res);
                this.getDataGridData();
            })
        });
    }

    getRemindInfoForPageRefresh(){
        this.remindService.getRemindInfo(this.pageId).then(res => {
            this.dgcService.setRemindColor(res);
        })
    }

    //获取datagrid数据
    getDataGridData() {
        this.getHeaderData();
        //this.getBodyData();
        // this.getFooterData();
        this.dgcService.abjustAfterView();
    }

    // 请求header数据
    getHeaderData() {
        //请求头部数据
        this.dataTableService.getColumnList(this.pageId);
        //为编辑模式准备列设置
        let json = {
            formId:'',
            table_id:this.pageId,
            is_view:0,
            parent_table_id: this.parentTableId||'',
            parent_real_id: this.parentRealId||'',
            parent_temp_id:this.parentTempId||''
        }
        this.dfService.getStaticFormData(json)
            .then(
                res => {
                    let myData = res.data;
                    for( let i = 0 ; i < myData.length ; i++)
                    {
                        if( myData[ i ].field_content )
                        {
                            if( myData[ i ].field_content.show_flow_node )
                            {
                                this.flow_node.push( {dfield:myData[ i ].dfield, limit:myData[ i ].field_content.limit,count_table:myData[ i ].field_content.count_table} );
                            }
                        }
                    }
                    this.getBodyData();
                }
            )
    }
    //节点统计穿透数据
    define_real:any;
    define_infos:any;
    //请求body数据
    getBodyData(isfresh?:any,theArr?) {
        if(this.isCreateFalseTable) {
            this.postData = {
                table_id: this.formValue['table_id'],
                data: this.formValue,
                field_id: this.fieldId,
                special_count_field_list:this.flow_node
            };
            this.dataTableService.getFlaseCountTable(this.postData).then(res => {
                let resRows = [];
                if (res.data.db.length != 0 && res.data.temp.length == 0) {
                    resRows = res.data.db
                }
                if (res.data.db.length == 0 && res.data.temp.length != 0) {
                    for (let obj of res.data.temp) {
                        obj['data'] = {};
                        obj['data']['status'] = 1;
                    }
                    resRows = res.data.temp
                }
                if (res.data.db != 0 && res.data.temp != 0) {
                    for(let obj of res.data.temp){
                        obj['data']={};
                        obj['data']['status']=1;
                    }
                    resRows = res.data.temp.concat(res.data.db);
                }
                //如果来自内联子表，则发布此消息，为了让内联子表去请求数据回显给父表
                if (this.songridFromInline == '1') {
                    this.globalService.getDataAfterGetGridData.next(this.pageId);
                }

                //统计字段查看数据显示footer
                if(res.data.footer){
                    let footer=[JSON.parse(JSON.stringify(res.data.footer))];
                    footer[0]['myfooter']="合计";
                    this.rowData_footer=footer;
                    this.cd.markForCheck();
                }

                this.rowData = this.dgcService.initRowData(resRows, this.parentTableId, this.songridFromInline);

                //固化数据
                this.isFixed = this.rowData[0] && this.rowData[0]["fixed"];
                this.total = res.total || 0;
                this.bodyDataStatus = true;
                this.setCorrespondenceLists(res, true);
                //订阅total
                this.dataGridMissionService.missionTotal(this.total);
                //点击刷新发的请求才显示刷新成功
                if (isfresh) {
                    this.setEditClickable();
                    this.openAlert("刷新成功", 'success');
                    this.isRefreshFinish.emit(true);
                }
                this.sortWay();
                try {
                    if (this.gridOptions_top.api) {
                        this.gridOptions_top.api.setRowData(this.rowData);
                        if (!this.autoResizeWidth) {
                            this.gridOptions_top.columnApi.autoSizeColumns(this.dgcService.getAllColumnIds(this.columnDefs));
                        }
                        this.gridOptions_top.api.refreshView();
                    }
                } catch (err) {
                }
                if (this.isShowGroup) {
                    setTimeout(() => {
                        this.setGroup();
                    }, 10)
                }
                //定时器为了使刷新前被勾选的行条目重新被选中，此方法应有优化空间，合理对刷新处理排序或可提升性能*****暂未优化*****
                setTimeout(() => {
                    //根据接收到的theArr(此数据为记录刷新前被选中的行条目ID，非必要参数)对行条目做选中处理
                    theArr && this.gridOptions_top.api.forEachNode((node) => {
                        this.dgcService.selectedRows(node, theArr)
                    })
                }, 0);
                this.cd.markForCheck();
            });
        }else {
        this.postData = this.dgcService.createPostData({
            keyword:this.keyword,
            subRowId:this.subRowId,
            subChildId:this.subChildId,
            subTableType:this.subTableType,
            subPageId:this.subPageId,
            parentTableId:this.parentTableId,
            parentRealId:this.parentRealId,
            parentTempId:this.parentTempId,
            queryParams:this.queryParams,
            viewMode:this.viewMode,
            rowId:this.rowId,
            tableType:this.tableType,
            fieldId:this.fieldId1,
            recordId:this.recordId,
            isBatch:this.isBatch,
            tempIdsForBatchInApprove:this.tempIdsForBatchInApprove,
            sourceTableId:this.source_table_id,
            tableIdForBuildin:this.tableIdForBuildin,
            baseBuildinDfield:this.base_buildin_dfield,
            isShowGroup:this.isShowGroup
        });
        if(this.flow_node.length > 0)
        {
            this.postData['special_count_field_list']=this.flow_node;
        }
        //请求表单数据
        this.dataTableService.getDataTableList(this.postData).then(res => {
            if (!res["success"]) {
                this.rowData = [];
                this.rowData_footer = [];
                this.total = 0;
                this.isLoading.emit( true );
                this.gridErr = res["error"];
            }

             //查看相关字段时伪造过滤条件
            if(this.isXiangguan){
                if(res.rows[0]) {
                    this.postDataFromXiangguan = {_id: res.rows[0]._id};
                }else{
            this.postDataFromXiangguan = null;
                }
            }

            //看是否加载常用查询
            if( res['common_filter_id'] && res['common_filter_id'] != 0 ){
                this.emitCommonQuery.emit( res['common_filter_id'] );
            }else {
                this.emitCommonQuery.emit( '' );
            }
            // if(this.gridOptions_top.api){
            //     this.gridOptions_top.api.setColumnDefs(this.columnDefs);
            // }
            let resRows = res.rows || [];
            //如果来自内联子表，则发布此消息，为了让内联子表去请求数据回显给父表
            if (this.songridFromInline == '1') {
                this.globalService.getDataAfterGetGridData.next(this.pageId);
            }

            this.rowData = this.dgcService.initRowData(resRows,this.parentTableId,this.songridFromInline);
            // this.rowData = [
            //     {'group':111,f1:'f1',f2:'f2',children:[
            //         {f1:'f1_1',f2:'f2_1'},
            //         {f1:'f1_2',f2:'f2_2'}
            //     ]},
            //     {'group':222,f1:'f1',f2:'f2',children:[
            //         {'group':'333',f1:'f1_1',f2:'f2_1',children:[
            //             {f1:'f1_1',f2:'f2_1'},
            //             {f1:'f1_2',f2:'f2_2'}
            //         ]},
            //         {f1:'f1_2',f2:'f2_2'}
            //     ]}
            // ]

            //固化数据
            this.isFixed = this.rowData[0] && this.rowData[0]["fixed"];
            this.total = res.total || 0;
            this.bodyDataStatus = true;
            this.setCorrespondenceLists(res,true);
            //订阅total
            this.dataGridMissionService.missionTotal(this.total);
            //点击刷新发的请求才显示刷新成功
            if(isfresh){
                this.setEditClickable();
                this.openAlert("刷新成功",'success');
                this.isRefreshFinish.emit(true);
            }
            this.sortWay();
            let myJson = {
                table_id : this.pageId,
                specical_count_field_infos : this.flow_node,
                rows : this.rowData
            };
            if ( this.flow_node.length > 0 ){
                this.dfService.getFlowNodeInfo(myJson)
                    .then(
                        res=>{
                            for( let i = 0 ; i < this.flow_node.length ; i++)
                            {
                                for( let j = 0 ; j < res.rows.length ; j++ )
                                {
                                    if( !res.define_infos[res.rows[j]._id] ){
                                        res.rows[ j ][ this.flow_node[i].dfield ] = res.rows[ j ][ this.flow_node[i].dfield ] == '' ? '' : '结束';
                                    }
                                    else if( !res.define_infos[ res.rows[ j ]._id ][ this.flow_node[i].dfield ] )
                                    {
                                        res.rows[ j ][ this.flow_node[i].dfield ] = res.rows[ j ][ this.flow_node[i].dfield ] == '' ? '' : '结束';
                                    }
                                }
                            }
                            this.rowData = res.rows;
                            this.define_real = res.rows;
                            this.define_infos = res.define_infos;
                            try{
                                if(this.gridOptions_top.api) {
                                    this.gridOptions_top.api.setRowData(this.rowData);
                                    if(!this.autoResizeWidth){
                                        this.gridOptions_top.columnApi.autoSizeColumns(this.dgcService.getAllColumnIds(this.columnDefs));
                                    }
                                    this.gridOptions_top.api.refreshView();
                                }
                            }catch (err){}
                            if( this.isShowGroup ){
                                setTimeout( ()=>{
                                    this.setGroup();
                                },10)
                            }
                            //定时器为了使刷新前被勾选的行条目重新被选中，此方法应有优化空间，合理对刷新处理排序或可提升性能*****暂未优化*****
                            setTimeout(()=>{
                                //根据接收到的theArr(此数据为记录刷新前被选中的行条目ID，非必要参数)对行条目做选中处理
                                theArr&&this.gridOptions_top.api.forEachNode((node)=> {this.dgcService.selectedRows(node,theArr)})
                            },0);
                            this.cd.markForCheck();
                        }
                    );
            }else {
                try {
                    if (this.gridOptions_top.api) {
                        this.gridOptions_top.api.setRowData(this.rowData);
                        if (!this.autoResizeWidth) {
                            this.gridOptions_top.columnApi.autoSizeColumns(this.dgcService.getAllColumnIds(this.columnDefs));
                        }
                        this.gridOptions_top.api.refreshView();
                    }
                } catch (err) {
                }
                if (this.isShowGroup) {
                    setTimeout(()=> {
                        this.setGroup();
                    }, 10)
                }
                //定时器为了使刷新前被勾选的行条目重新被选中，此方法应有优化空间，合理对刷新处理排序或可提升性能*****暂未优化*****
                setTimeout(()=> {
                    //根据接收到的theArr(此数据为记录刷新前被选中的行条目ID，非必要参数)对行条目做选中处理
                    theArr && this.gridOptions_top.api.forEachNode((node)=> {
                        this.dgcService.selectedRows(node, theArr)
                    })
                }, 0);
                this.cd.markForCheck();
            }
        });
        }
    }

    firstSetState: boolean = true;

    setCorrespondenceLists(res,isSelectedList){
        //对应关系回显的ids
        if (isSelectedList&&res.selectedList) {
            this.correspondenceSelectedList = res.selectedList;
        }
        //对应关系增加的ids
        if (res.addList) {
            this.correspondenceAddList = res.addList;
        }
        //对应关系减少的ids
        if (res.removeList) {
            this.correspondenceRemoveList = res.removeList;
        }
    }

    @Input() isXiangguan:boolean=false;
    @Input() postDataFromXiangguan:any;
    //请求footer数据
    getFooterData() {
        let obj = this.postData;
        if ( this.parentTableId ) {
            // let filtername = 'section_page_' + this.parentTableId;
            // var b = {};
            // if ( this.parentTempId || this.parentRealId ) {
            //     b[filtername] = this.parentTempId || this.parentRealId;
            // }
            // obj = {
            //     // filter: b,
            //     table_id: this.pageId || "",
            //     parent_table_id: this.parentTableId || "",
            //     parent_temp_id: this.parentTempId || "",
            //     parent_real_id: this.parentRealId || "",
            //     tableType: this.tableType || "",
            //     rowId: this.rowId || "",
            //     fieldId: this.fieldId1 || "",
            //     childInfo: {
            //         "parent_page_id": this.parentTableId,
            //         "parent_row_id": this.parentTempId || this.parentRealId
            //     },
            // };
        }else if( this.isBatch ){
            obj = this.dgcService.setBatch(this.tempIdsForBatchInApprove,this.isBatch);
        }else if(this.queryParams['_id']){
            obj = {
                table_id:this.pageId,
                mongo:this.queryParams
            }
        } else {
            obj = { table_id: this.pageId };
        }

        if(this.keyword){
            obj['tableType']='keyword';
        }

        obj = this.dgcService.getSearchAndSort( obj,this.tableType,this.keyword,this.isShowGroup,this.myGroup );

        //相关字段伪造的过滤条件
        let isXG:any=false;
        if (this.isXiangguan) {
            if(this.postDataFromXiangguan) {
                isXG = this.postDataFromXiangguan;
            }else{
                return
            }
        }

        this.dataTableService.getFootDataWithoutCache( obj, isXG )
            .then(res => {
                //获得fooetr同时获取分组函数相关数据
                this.AggList = [];
                this.rowData_footer = this.dgcService.createFooterData(res,this.AggList,this.columnDefs_bottom);
                this.cd.markForCheck();
            })
    }

    ngAfterViewInit() {
        // this.oHandle = this.domHandler.findSingle(this.el.nativeElement, 'div.data-grid-handle');
        // this.documentClickListener = this.renderer.listenGlobal('body', 'click', (event) => {
        //     if (event.target.childNodes[0] == this.oHandle) {
        //         return;
        //     }
        //     this.oHandle.style.display = 'none';
        // });
        //
        // let ag_grid = this.el.nativeElement.querySelector(".ag-body-viewport");
        // $( ag_grid ).click( ()=>{
        //     this.isShowCustomColumns = false;
        // } )
    }

    // 前端渲染时间,勿删,可能会出现页面加载后冻结的现象
    getFrontTime() {
    }

    //设置字体为保存的字号
    onColumnEverythingChanged($event) {
        this.gridFont = this.dgcService.setFontStyle();
    }

    //组装columns
    createColumnDefs() {
        // this.createHeaderColumnDefs(false);
        this.columnDefs = this.dgcService.createHeaderColumnDefs(this.emitReset,
        this.oHandle,this.isFixed,this.permission['edit'],this.customOperateList,this.rowOperation,
        this.fieldContent,this.tableType,this.source_field_dfield,this.viewMode,false);
        this.dgcService.columnEditDefs = this.dgcService.createHeaderColumnDefs(this.emitReset,
            this.oHandle,this.isFixed,this.permission['edit'],this.customOperateList,this.rowOperation,
            this.fieldContent,this.tableType,this.source_field_dfield,this.viewMode,false,true);
        if( this.firstSetState ){
            setTimeout( ()=>{
                this.gridOptions_top.api.setColumnDefs( this.columnDefs );
                this.getOrSetGridState( true,true );
                this.gridOptions_top.columnApi.setColumnState( this.dgcService.calcColumnState() )
                this.firstSetState = false;
                this.isLoading.emit(true);
            },0 )
        }
    }

    createColumnDefByType(defs_filter:any, type:string) {
        defs_filter = [];
        for (let col of this.dgcService.cols) {
            this.createAColumn(defs_filter,col,type);
        }

        this.dgcService.addDefaultColumn(defs_filter);
        if(type == "Bottom"){
            this.columnDefs_bottom = defs_filter;
        } else if(type == "Filter"){
            this.columnDefs_filter = defs_filter;
        }
    }

    createAColumn(defs_filter:any, col:any, type:string){
        let This =this;
        //id列不添加多级索引
        if (col["field"] == "_id" || col['dtype'] == 9) {
            return;
        }
        let obj;
        if(type == "Filter"){
            obj = this.dgcService.setFilterColumn(col);
            obj["headerCellTemplate"]=function(data){
                return This.createFilter(This.dgcService.getFilterSetting(col,{'data': data,'fn': This.startFilter}));
            }
        } else if (type == "Bottom"){
            obj = this.dgcService.setBottomColumn(col);
        }
        this.dgcService.setColumnWidth(obj,col);
        defs_filter.push(obj);
    }

    //返回nodeChildDetails
    getNodeChildDetails ( rowItem ){
        if ( rowItem.group||Object.is(rowItem.group,'')||Object.is(rowItem.group,0) ) {
            return {
                group: true,
                // open C be default
                // expanded: rowItem.group === 'Group C',
                expanded: rowItem.children&&rowItem.children[0]&&rowItem.children[0].group?true:false,
                // provide ag-Grid with the children of this group
                children: rowItem.children,
                // this is not used, however it is available to the cellRenderers,
                // if you provide a custom cellRenderer, you might use it. it's more
                // relavent if you are doing multi levels of groupings, not just one
                // as in this example.
                field: 'group',
                // the key is used by the default group cellRenderer
                key: rowItem.group
            };
        } else {
            return {
                group: false
            };
        }

    }

    //分组打开关闭
    onColumnRowGroupChanged($event){
        console.log( "@@@@@@@@@@@" )
        console.log( "@@@@@@@@@@@" )
        console.log( $event )
    }

    //获取选中的row
    onRowSelected(event) {
        if(this.isEditable){
            return;
        }
        if( !event["node"]["data"] ){
            return;
        }

        this.correspondenceSelectedList=this.dgcService.setcorrespondenceSelectList(event.node.selected,this.correspondenceSelectedList,event.node.data._id);
        //出现行选中状态时想父组件弹射被选中的行条目数据ID
        this.selectedArr.emit(this.correspondenceSelectedList);
        if( this.viewMode == 'editFromCorrespondence' ){
            this.dataGridMissionService.correspondenceIdsNext( this.correspondenceSelectedList );
        }

        // node.setSelected(!node.selected, false);
        //个人用户编辑表中的数据时,会发起对应的工作流,不让用户编辑
        if( event["node"]["data"]["status"] && ( event["node"]["data"]["status"] == 2 ) ){
            this.openAlert("该数据正在审批，无法操作。", 'success');
            event["node"].setSelected( false,false );
            this.cd.markForCheck();
            return;
        }
        //数据计算cache时,不让用户编辑
        if( event["node"]["data"]["data_status"] && ( event["node"]["data"]["data_status"] == 0 ) ){
            this.openAlert("数据计算中，请稍候", 'success');
            return;
        }
        this.dgcService.setdeleteLists(event,this.deleteListRel,this.deleteListTemp);
        this.dataGridMissionService.missionDeleteList({
            pageId: this.pageId,
            deleteListTemp: this.deleteListTemp,
            deleteListRel: this.deleteListRel
        });
        //点击checkbox时右键的menu隐藏
        let menu = this.domHandler.findSingle(this.el.nativeElement,'div.ag-menu');
        if(menu){
            menu.style.display='none';
        }
    }

    //全选判断用（是否在屏幕内）
    onVirtualColumnsChanged ($event){
        if(this.isEditable){
            return;
        }
        let cbox = this.domHandler.findSingle(this.el.nativeElement,'input.header_input');
        if( cbox ){
            cbox.checked = !this.dgcService.isSelect;
        }
    }

    clearDelete(){
        this.deleteListTemp = [];
        this.deleteListRel = [];
        this.dataGridMissionService.missionDeleteList({
            pageId: this.pageId,
            deleteListTemp: this.deleteListTemp,
            deleteListRel: this.deleteListRel
        })
    }

    //更换sheet页面,event参数由my-paginator组件抛出，由data-table-page-data接受，并传入到本组件的该方法中
    changeSheet(event) {
        let state = this.gridOptions_top.columnApi.getColumnState();
        let arr_1 = this.dgcService.getColumnState(event,state);
        this.setColumnState( arr_1 );
    }

    // 全选按钮状态转换
    unSelect(){
        let cbox = this.domHandler.findSingle(this.el.nativeElement, 'input.header_input');
        this.dgcService.isSelect = true;
        if(cbox){
            cbox.checked = false;
        }
    }

    //排序
    onSortChanged($event) {
        // if(this.isEditable){
        //     return;
        // }
        if( this.viewMode == 'viewFromCorrespondence' || this.viewMode == 'editFromCorrespondence' ){
            return;
        }
        let data = this.gridOptions_top.api.getSortModel()[0];
        this.postData = {
            first: 0,
            rows: this.dgcService.rows,
            table_id: this.pageId || ""
        }
        this.dgcService.sortChangedPostData(data,this.postData,this.rowId,
            this.parentTableId,this.parentTempId,this.parentRealId,this.tableType,this.fieldId1);

        this.dgcService.sortDataState = {
            sortOrder:this.postData['sortOrder'],
            sortField:this.postData['sortField'],
            sort_real_type:this.postData['sort_real_type']
        }
        this.postData = this.dgcService.getSearchAndSort( this.postData,this.tableType,this.keyword,this.isShowGroup);
        //如果是后端排序在进行查询
        if (!this.dgcService.sortBtn){
            this.justSearch( this.postData );
        }
        if( this.dgcService.sortBtn ){
            this.gridOptions_top.api.setRowData( this.rowData );
        }
    }

    onEmitGroupData ($event){
        this.myGroup = $event;
        this.groupUpdate();
    }

    //分组操作
    // 清空Group
    clearGroup() {
        this.myGroup = [];
        this.dataTableService.savePreference({
            'action': 'group',
            'table_id': this.pageId,
            'group': JSON.stringify([])
        });
        this.showData();
        this.isShowGroup = false;
    }

    //添加删除Group
    changeGroup($event) {
        if(this.isEditable){
            return;
        }
        this.showGroup();
        let colId = '';
        if( $event.columns.length>=1 ){
            colId = $event.columns[$event.columns.length-1].colId || '';
        }
        this.myGroup = [];
        for( let data of $event.columns ){
            this.myGroup.push( data.colId );
        }

        //请求分组数据
        this.groupUpdate();

        //使数据折叠显示用
        this.gridOptions_top['groupDefaultExpanded'] = this.myGroup.length == 0 ? 0 : this.myGroup.length - 1;
        this.dataTableService.savePreference({
            'action': 'group',
            'table_id': this.pageId,
            'group': JSON.stringify(this.myGroup)
        });
        this.isShowGroup = true;
        //展开全部Group
        // this.gridOptions_top.api.expandAll();
        if( colId != '' ){
            this.gridOptions_top.columnApi.setColumnVisible( colId,true );
        }
        //如果没有分组数据 第一次添加分组同时打开函数
        if( this.isNewAgg ){
            this.isNewAgg = false;
            this.addAggFun();
        }
        //使数据折叠显示用
        this.gridOptions_top.api.refreshInMemoryRowModel();
        this.cd.markForCheck();
    }

    // 显示Group
    showGroup() {
        if (!this.isShowGroup) {
            this.isShowGroup = true;
            //使数据折叠显示用
            this.gridOptions_top['groupDefaultExpanded'] = this.myGroup.length == 0 ? 0 : this.myGroup.length - 1;
            this.getOrSetGridState( true ,true);
            let i = 0;
            if (this.myGroup.length != 0) {
                this.groupUpdate();
            } else {
                this.openAlert("没有分组数据,请创建。",'confirm');
                this.cd.markForCheck();
                this.gridOptions_top.api.showToolPanel(true);
                this.isNewAgg = true;
            }
            //展开全部Group
            // this.gridOptions_top.api.expandAll();
            this.showGroupAndCustom( true );
            this.cd.markForCheck();
        }
    }

    //设置分组
    setGroup (){
        let state = this.dgcService.setGroup(this.gridOptions_top.columnApi.getColumnState(),this.myGroup);
        this.gridOptions_top.columnApi.setColumnState( state );
        this.gridOptions_top.api.showToolPanel(true);
        this.addAggFun();
        this.isShowGroup = true;
        this.showGroupAndCustom( true );
        this.cd.markForCheck();
    }

    //是否需要添加函数
    isNewAgg: boolean = false;
    AggList: any = [];

    //添加函数
    addAggFun (){
        this.gridOptions_top.columnApi.addValueColumns( this.AggList );
    }

    // 显示数据
    showData() {
        if (this.isShowGroup) {
            this.isShowGroup = false;

            let postData = this.postData;
            delete postData['is_group'];
            postData['tableType'] = this.tableType;
            delete postData['group_fields'];
            this.justSearch( postData );

            // this.getOrSetGridState( false,true );
            //关闭聚合
            // this.gridOptions_top.columnApi.addValueColumns( [] );
        }
        this.gridOptions_top.api.showToolPanel(false);
    }
    //显示左侧工具栏
    custom() {
        if(this.isShowGroup){
            this.showData();
            //当在分组下直接点击定制列时向上通知将分组关闭
            this.emitCloseGroup.emit( true );
        }
        this.gridOptions_top.api.showToolPanel(!this.gridOptions_top.api.isToolPanelShowing());
        let agGrid =  this.el.nativeElement.querySelectorAll("ag-grid-angular");
        this.renderer.setElementStyle( agGrid[0], "padding-right", this.gridOptions_top.api.isToolPanelShowing() ? "200px" : '' );
        this.showGroupAndCustom( false );
        //==点开定制列的时候==
        let contain =  this.el.nativeElement.querySelectorAll(".ag-bl-center");
        contain[1].addEventListener( 'click', (event) => {
            if(!this.isShowGroup && this.gridOptions_top.api.isToolPanelShowing()){
            this.gridOptions_top.api.showToolPanel(false);
            let agGrid =  this.el.nativeElement.querySelectorAll("ag-grid-angular");
            this.renderer.setElementStyle( agGrid[0], "padding-right", false ? "200px" : '' );
            this.showGroupAndCustom( false );
            }
        });
    }

    //获取隐藏列
    onColumnVisible(event) {
        // if(this.isEditable){
        //     return;
        // }
        // if( this.isShowGroup ){
        //     return;
        // }
        // let hideColumn=this.dgcService.setHideColumn(this.gridOptions_top.columnApi.getColumnState(),true);
        // this.dataTableService.savePreference({
        //     'action': 'ignoreFields',
        //     'table_id': this.pageId,
        //     'ignoreFields': JSON.stringify( hideColumn )
        // });
        // this.makeSameOrder();
    }
    //是否进行了宽度设置
    isSetWidth: boolean = false;

    columnResized (){
        this.dgcService.setResizedColumnWidth(this.gridOptions_top.columnApi.getColumnState());
        this.isSetWidth = true;
        let saveWidth = setInterval( ()=>{
            if( this.isSetWidth ){
                this.saveColWidth();
                this.isSetWidth = false;
            }
            clearInterval( saveWidth );
        },2000 )
    }

    //保存列宽
    saveColWidth (){
        let obj = this.dgcService.getResizedColumnWidth();
        if(!obj){
            return;
        }
        this.dataTableService.savePreference({
            'action': 'colWidth',
            'table_id': this.pageId,
            'colWidth': obj
        });
    }

    onColumnMoved ($event){
        if(this.isEditable){
            return;
        }
        //解决拖动之后footer以及filter不对应的问题
        this.gridOptions_top.api.setRowData(this.rowData);
    }

    onDragStarted ($event){
        if(this.isEditable){
            return;
        }
        //提高拖动提示的层级使其显示
        setTimeout( ()=>{
            $(".ag-dnd-ghost").css( {'z-index':'999'} )
        },200 )
    }

    //拖动保存的数据
    dragArr: any = [];
    //是否保存拖动数据
    isSaveDrag: boolean = false;

    //获取头部排序后数组
    onDragStopped($event) {
        // if(this.isEditable){
        //     return;
        // }
        if( this.isShowGroup ){
            return;
        }
        let state = this.gridOptions_top.columnApi.getColumnState();
        this.dragArr = this.dgcService.getColumns(state);
        if(this.dragArr.join('') != this.dgcService.orderFields.join('')){
            this.isSaveDrag = true;
        }
        if( this.isSaveDrag ){
            this.dataTableService.savePreference({
                'action': 'fieldsOrder',
                table_id: this.pageId,
                fieldsOrder: JSON.stringify( this.dragArr )
            });
            this.isSaveDrag=false;
        }
        try{this.CustomColumnsComponent.makeSameState();}catch(e){}
    }

    //图片数据
    imgData:any;
    //图片预览弹窗
    imgDisplay:boolean = false;
    //选择的图片
    imgSelect:any = "";
    //图片loading动画
    imgLoading:boolean = true;
    //图片附件删除
    imgDel(val) {
        this.dataTableService.delFileUpload([val]).then(res => {
            console.log("删除后返回的值",this.selector);
            console.log(res,this.selector);
        })
    }
    //上一张图片选择
    imgChange(num) {
        let imgNum = this.dgcService.getImageNum(num);
        this.imgSelect = this.imgData.rows[imgNum].file_id;
        this.dgcService.setImgData(this.imgData);
        this.setImgSize();
    }

    imgClickChange(val) {
        let imgNum = this.dgcService.setImageNum(this.imgData,val)||this.dgcService.imgNum;
        this.imgSelect = this.imgData.rows[imgNum].file_id;
        this.dgcService.setImgData(this.imgData);
        this.setImgSize();
    }

    //图片预览调整尺寸
    setImgSize() {
        let This = this;
        this.imgLoading = true;
        setTimeout(function () {
            This.dgcService.sizeImageWin();
            This.imgLoading = false;
            This.cd.markForCheck();
        }, 200);
    }

    //关闭图片弹窗调用
    closeImgWin (){
        this.imgSelect = "";
    }

    onRowClicked ($event){
        if(this.isEditable){
            return;
        }
        // let node = $event.node;
        // node.setSelected(!node.selected, false);
    }

    //点击子表时不能双击
    cannotDoubleClick: boolean = false;

    //富文本字段弹窗
    textDisplay:boolean=false;
    textValue:string=''

    //对应关系单击通知改变viewMode
    @Output() correspondenceView=new EventEmitter<any>();

    //全景图title
    cycleTitle : string = "";
    lifeCycleData;
    //datagrid子表弹窗
    onCellClicked(data) {
        if( !data.data ){
            return;
        }
        this.cycleTitle = data.data.f9;
        //分组重新渲染序号
        let groupValue = data.data.group;
        if( (groupValue||Object.is( groupValue,'' )||Object.is( groupValue,0 ))&&data.colDef.field=='group' ){
            this.gridOptions_top.api.refreshView();
        }
        let arr=[];
        for(let obj of data.columnApi._columnController.primaryColumns){
            if(obj['colDef']['count_table_id']){
                arr.push(obj);
            }
        }

        this.col_id=data.data._id;
        this.colDef=arr;
        if(data.colDef.headerName != "操作"){
            let ele=data.event.target;
            let node = data.node;
            if(ele.className.indexOf("my-ag-cell-focus2")!=-1){//第三次点击
                node.setSelected(false, false);
                this.domHandler.removeClass(ele, 'my-ag-cell-focus2');
                this.cd.markForCheck();
            }else if(ele.className.indexOf("my-ag-cell-focus1")!=-1){//第二次点击
                node.setSelected(true, false);
                this.domHandler.removeClass(ele, 'my-ag-cell-focus1');
                this.domHandler.addClass(ele,'my-ag-cell-focus2');
                this.cd.markForCheck();
            }else{//第一次点击
                this.domHandler.addClass(ele,'my-ag-cell-focus1');
            }
        }
        if(this.isEditable){
            return;
        }
        if( !data.data ){
            return;
        }
        this.status = data.data.status || "";
        if (data.data) {
            this.realId = data.data._id;
        } else {
            this.realId = 0;
            return;
        }
        //数据计算cache不可操作
        if( data["data"]["data_status"] &&  data["data"]["data_status"]=='0'){
            this.openAlert("数据计算中，请稍候",'success');
            return;
        }
        if( data.colDef.dinput_type == 23 ){
            let json = {};
            json["dfield"] = data.colDef.field;
            json["table_id"] = this.pageId;
            json[(data.data.action?"temp_id":"real_id")] = data.data._id;
            this.dataTableService.getImgData( json ).then( res => {
                let obj=this.dgcService.setImgDataAndNum(res,this.imgData,this.imgSelect);
                this.imgData=obj.imgData;
                this.imgSelect=obj.imgSelect;
                this.imgDisplay = true;
                this.setImgSize();
            } )
        }
        //点击富文本字段
        if( data.colDef.dinput_type == 2 ){
            this.textDisplay = true;
            this.textValue=data.value;
            $("#myGrid .ag-body-viewport").attr("tabindex",1);
            $("#myGrid .ag-body-viewport").css('outline','none')
            $("#myGrid .ag-body-viewport").focus();
        }

        //合同编辑器字段
        if(data.colDef.real_type==34){
            this.values=data.value;
            this.ids={table_id:this.pageId,real_id:data.data._id,field_id:data.colDef.id};
            if(data.event.target.innerHTML=="下载"){
                console.log('下载');
                this.isDownloadContract(data);
            }
            if(data.event.target.innerHTML=="查看"){
                console.log('查看');
                this.showContract(data);
            }
        }

        if(( data.colDef.real_type == 9||data.colDef.real_type == 33 ) && data.event.srcElement.id == 'file_view'){
            //点击附件字段
            //let field = data.colDef.field;
            this.fieldids = data['value']
            this.file_dinput_type = data.colDef.real_type;
            // this.realId = data['data']['_id']
            this.isShowEnclosure = true;
        }

        //内置相关查看原始数据用
        if( data.event.srcElement.id == 'relatedOrBuildin' ){
            if( data.colDef.is_user ){
                this.globalService.openUserInfo.next( data.value );
                setTimeout( ()=>{
                    this.globalService.openUserInfo.next( null );
                },1000 )
            }else {
                this.dgcService.requestBuildinSource(data);
            }
        }

        if( this.datagridStatus ){ //查看模式下
            //对应关系单击弹窗
            if(data.colDef.dinput_type == 16 && data.value.toString().length && data.event.target.id == "correspondenceClick"){
                this.dgcService.requestCorrespondenceSource(data,this.correspondenceView);
            }
            //加data.event.target.id == "childOrCount"判断是为了避免范围选择时触发子表或者统计弹窗
            if((this.dgcService.childTable(data.colDef.dinput_type)||this.dgcService.countTable(data.colDef.dinput_type,data.colDef.real_type))&&data.value.toString().length && data.event.target.id == "childOrCount"){
                //点击子表以及统计时不触发表单查看事件
                this.cannotDoubleClick = true;
                setTimeout( ()=>{
                    this.cannotDoubleClick = false;
                },1000 )
                //判断是否为对应关系
                this.dgcService.doCorrespondence(this.viewMode,data);
            }else if (data.colDef.headerName == "操作") {//操作
                this.gridHandle(data);
            }
        }
    }

    //选择范围发生变化
    rangeSelectionChanged (event){
    }

    //显示隐藏filter
    showFilter() {
        this.isShowFilter = !this.isShowFilter;
        this.dgcService.showFilter(this.isShowFilter);
    }

    //显示定制列以及分组
    showGroupAndCustom ( isGroup ){
        let hideCol =  this.el.nativeElement.querySelectorAll(".ag-column-select-panel");
        this.dgcService.showGroupAndCustom(hideCol,isGroup,this.gridOptions_top);
        //固定列功能
        // if( !isGroup ){
        //     this.createFix();
        //     //使panel与列顺序相同
        //     this.makeSameOrder();
        // }
        //
        // //拖拽功能
        // if( !isGroup ){
        //     $(hideCol[0]).mouseover( ()=>{
        //         this.panelDrag();
        //     } );
        // }
    }

    //使panel与列顺序相同
    makeSameOrder (){
        // let state = this.gridOptions_top.columnApi.getColumnState();
        // this.dgcService.reorderFooterConbineWithGrid(state,this.columnDefs);
        this.CustomColumnsComponent.makeSameState();
    }

    // startPosition;
    // moveColDiv;
    // isColMove;
    // moveColIndex;
    // toDivIndex;

    //右侧拖拽
    // panelDrag (){
    //     if( this.isShowGroup ){
    //         return;
    //     }
    //     let selectCols = this.el.nativeElement.querySelectorAll('.ag-column-select-column');
    //     $('.ag-column-select-column').mousedown( ($event)=>{
    //          this.startPosition = {
    //              x: $event.clientX,
    //              y: $event.clientY
    //          }
    //         this.moveColDiv = $event.currentTarget;
    //         this.isColMove = true;
    //
    //         document.onmousemove = (event)=>{
    //             if( this.isColMove ){
    //                 let y = event.clientY;
    //                 if( Math.abs(y - this.startPosition.y)<40 ){
    //                     return;
    //                 }
    //                 let num = Math.floor(Math.abs(y - this.startPosition.y)/30);
    //                 for( let i = 0;i<selectCols.length;i++ ){
    //                     $( selectCols[i] ).removeClass( 'moveto-bottom' );
    //                     $( selectCols[i] ).removeClass( 'moveto-top' );
    //                     if( $(this.moveColDiv)[0]&&$( selectCols[i] )[0]&&$( selectCols[i] )[0].children[2].innerHTML == $(this.moveColDiv)[0].children[2].innerHTML ){
    //                         this.moveColIndex = i;
    //                     }
    //                 }
    //                 if( y>this.startPosition.y ){
    //                     this.toDivIndex = this.moveColIndex + num;
    //                 }else {
    //                     this.toDivIndex  = this.moveColIndex - num;
    //                 }
    //                 if( this.toDivIndex>=selectCols.length ){
    //                     this.toDivIndex = selectCols.length - 1;
    //                 }
    //                 if( y>this.startPosition.y ){
    //                     $( selectCols[this.toDivIndex] ).addClass( 'moveto-bottom' );
    //                 }else {
    //                     if( this.toDivIndex>=0 ) {
    //                         $( selectCols[this.toDivIndex] ).addClass( 'moveto-top' );
    //                     }else {
    //                         $( selectCols[0] ).addClass( 'moveto-top' );
    //                     }
    //                 }
    //             }
    //         }
    //     } )
    //
    //     document.onmouseup = ($event)=>{
    //         if( this.startPosition == null || this.moveColDiv == null
    //             || this.moveColIndex == null || this.toDivIndex == null || this.isShowGroup ){
    //             this.moveColDiv = null;
    //             return;
    //         }
    //         if( this.isColMove ){
    //             this.isColMove = false;
    //             let state = this.gridOptions_top.columnApi.getColumnState();
    //             if( this.toDivIndex>=0 ){
    //                 this.gridOptions_top.columnApi.setColumnState(
    //                     this.dgcService.calcColumnStateOnColOrder(this.moveColIndex,this.toDivIndex,state));
    //             }else {
    //                 let obj = state[this.moveColIndex];
    //                 state.splice( this.moveColIndex,1 );
    //                 state.unshift( obj );
    //                 this.gridOptions_top.columnApi.setColumnState( state );
    //             }
    //             document.onmousemove = null;
    //             document.onmouseup = null;
    //             this.startPosition = null;
    //             this.moveColDiv = null;
    //             this.moveColIndex = null;
    //             this.toDivIndex = null;
    //             this.dgcService.createHidefilter(this.gridOptions_top);
    //             this.createFix();
    //             this.makeSameOrder();
    //             setTimeout( ()=>{
    //                 this.onDragStopped('');
    //             },100)
    //         }
    //     }
    // }
    //
    // sortState;
    //
    // //创建固定列
    // createFix(){
    //     this.sortState=this.gridOptions_top.api.getSortModel();
    //     let selectLabel =  this.el.nativeElement.querySelectorAll( ".ag-column-select-column" );
    //     let length = this.columnDefs.length;
    //     let fixColsR = this.dgcService.fixCols.r;
    //     let fixColsL = this.dgcService.fixCols.l;
    //     for( let i = 0;i <= length;i++ ){
    //         if( !selectLabel[i] ){
    //             continue;
    //         }
    //         let colName = selectLabel[i].children[2].innerHTML;
    //         let sysCols = ['序号','操作','勾选框'];
    //         if( sysCols.indexOf( colName ) != -1 ){
    //             continue;
    //         }
    //         this.renderer.setElementStyle( selectLabel[i], "position", "relative" );
    //         let spanL = document.createElement( 'span' );
    //         spanL.className = fixColsL.indexOf( this.dgcService.colNameToField[colName] ) == -1 ? 'fixBtn fixBtn-l' : 'fixBtn fixBtn-l fixSelect-l';
    //
    //         let spanR = document.createElement( 'span' );
    //         spanR.className = fixColsR.indexOf( this.dgcService.colNameToField[colName] ) == -1 ? 'fixBtn fixBtn-r' : 'fixBtn fixBtn-r fixSelect-r';
    //
    //         spanL.addEventListener( 'click',()=>{
    //             this.selectFix( spanL,spanR,true,colName )
    //         } )
    //         spanR.addEventListener( 'click',()=>{
    //             this.selectFix( spanR,spanL,false,colName )
    //         } )
    //
    //         selectLabel[i].appendChild(spanL);
    //         selectLabel[i].appendChild(spanR);
    //     }
    // }
    //
    //
    // //选择固定列
    // selectFix( obj,another,isL,colName ){
    //     this.dgcService.calcColumnDefsAndfixCols(this.columnDefs,obj,another,colName,isL);
    //     this.gridOptions_top.api.setColumnDefs( this.columnDefs );
    //     this.setFixState();
    //     this.showGroupAndCustom( false );
    // }
    //
    // //有固定列的state设置
    // setFixState (){
    //     let state = this.gridOptions_top.columnApi.getColumnState();//
    //     let stateArr =  this.dgcService.calcFixArr(state);
    //     this.setColumnState(stateArr);
    //     this.dgcService.createHidefilter(this.gridOptions_top);
    //     //保存
    //     this.dataTableService.savePreference({
    //         'action': 'pinned',
    //         table_id: this.pageId,
    //         pinned: JSON.stringify( this.dgcService.fixCols )
    //     });
    //     if(this.dgcService.sortBtn ){
    //         this.gridOptions_top.api.setSortModel(this.sortState);
    //     }
    //     this.makeSameOrder();
    // }

    //根据数据数量选择排序方式
    sortWay() {
        if (this.headerDataStatus&&this.bodyDataStatus) {
            let sortBtn = this.dgcService.sortWay(this.total);
            if( this.viewMode == 'viewFromCorrespondence' || this.viewMode == 'editFromCorrespondence' ){
                sortBtn = true;
                this.dgcService.sortBtn = true;
                console.log("对应关系前台排序",this.selector)
            }
            this.initGridOptions(sortBtn);
            //初始化字体设置
            this.gridFont = this.dgcService.setFontStyle();
            this.isFilterShow = true;
        }
    }

    //初始化表格设置
    initGridOptions(sortBtn) {
        let imgUrl = require('../slide-sidebar/online-sidebar/icon_shuju_no.png');
        let localText = {
            groups: '分组',
            rowGroupColumnsEmptyMessage: '拖拽至此以分组',
            copy: '复制',
            ctrlC: 'ctrl + C',
            paste: '粘贴',
            ctrlV: 'ctrl + V',
            noRowsToShow: this.gridErr=='' ? '<img src=' + imgUrl + '>' : this.gridErr
        };
        this.gridOptions_top["enableServerSideSorting"] = !sortBtn;
        this.gridOptions_top["enableSorting"] = sortBtn;
        // this.gridOptions_top["enableFilter"] = sortBtn;
        // this.gridOptions_top["EnableServerSideFilter"] = !sortBtn;
        this.gridOptions_top["enableRangeSelection"] = true;
        this.gridOptions_top["toolPanelSuppressRowGroups"] = false;
        this.gridOptions_top["getContextMenuItems"] = this.dgcService.getContextMenuItems;
        this.gridOptions_top["localeText"] = localText;

        //高级查询订阅参数
        this.dataGridMissionService.missionSearch({cols: this.dgcService.cols, rows: this.dgcService.rows});
    }

    //宽度自适应之前保存状态
    colStates: any;

    //datagrid宽度自适应
    autoSize() {
        if (this.autoResizeWidth) {
            this.colStates = this.gridOptions_top.columnApi.getColumnState();
            this.autoResizeWidth = false;
            this.gridOptions_top.columnApi.autoSizeColumns(this.dgcService.getAllColumnIds(this.columnDefs));
        } else {
            this.autoResizeWidth = true;
            let arr = this.dgcService.getOldColumnWidth(this.gridOptions_top.columnApi.getColumnState(),this.colStates);
            this.setColumnState( arr );
            //创建搜索以及固定列
            this.dgcService.createHidefilter(this.gridOptions_top);
            // this.createFix();
            this.dgcService.setResizedColumnWidth(arr);
        }
    }

    //仅显示对应关系勾选项
    onlyShowCorrespondenceData(){
        this.showAllCorrespondenceData(this.dgcService.getCorrespondenceRows(this.rowData,this.correspondenceSelectedList));
    }

    API: any;

    //显示所有对应关系数据
    showAllCorrespondenceData(arrCorr:any){
        if( this.API ){
            this.gridOptions_top.api = this.API;
        }else {
            this.API = this.gridOptions_top.api;
        }
        this.gridOptions_top.api.setRowData(arrCorr);
        //对应关系的回显勾选
        this.gridOptions_top.api.forEachNode( (node) => {
            if( !node["data"] ){//处理在group中，报错
                return;
            }
            let id = node["data"]["_id"];
            if(this.correspondenceSelectedList.indexOf(id) != -1){
                node.setSelected(true);
            }
        });
        this.gridOptions_top.api.refreshView();
        this.cd.markForCheck();
    }

    //获得以及设置状态
    getOrSetGridState( isGet: boolean ,isshowgrip){
        if( isGet){
            if(isshowgrip){
                try{
                    this.dgcService.gridState = this.gridOptions_top.columnApi.getColumnState();
                }catch (e){}
            }
        }else{
            this.setColumnState(this.dgcService.gridState);
         }
    }

    //确认修改
    viewOrEditConfirm() {
        this.changeWindow = true;
    }

    //修改数据保存
    saveChangeValue() {
        this.changeWindow = false;
    }

    doRowOperation(r,$event){
        if( r['frontend_addr'] !== ''){
            //执行前端操作
            this.rowOperationFrontend({
                rowId:this.realId,
                table_id:this.pageId,
                frontendAddress:r['frontend_addr'],
                row_op_id:r['row_op_id']
            });
        }else if(r['pyscript_addr'] !== ''){
            //执行后端操作
            this.rowOperationBackend({
                table_id:this.pageId,
                selectedRows:JSON.stringify([$event['data']['_id']])
        },'data'+r['pyscript_addr']);
        }
    }

    //表单弹窗 编辑
    onDataGridEdit($event) {
        //个人用户编辑表中的数据时,会发起对应的工作流,不让用户编辑
        if (this.status == 2) {
            this.openAlert("该数据正在审批，不能编辑。",'success');
            return;
        }
        if( this.namespace == 'external' ) {
            this.openAlert("该表为外部数据表，不可编辑。",'success');
            return;
        }
        if (this.realId == 0) {
            return;
        }
        this.isView = 0;
        this.display = true;
        this.btnDisplay = 'tijiao';
    }

    //表单弹窗 查看
    onDataGridView($event) {
        if (this.realId == 0) {
            return;
        }
        if( this.namespace == 'external' ) {
            this.openAlert("该表为外部数据表，不可查看。",'success');
            return;
        }
        this.setDisplay(1,true,"",(this.status != 2));
    }

    setDisplay(is_view:number, display:boolean,btn_display?:string, extra_condition?:boolean){
        this.isView = is_view;
        this.display = display;
        this.btnDisplay = this.dgcService.setBtnDisplay(this.viewMode,this.isFixed,this.permission['edit'],this.rowStatus, btn_display, extra_condition);
    }

    colDef={};

    col_id='';

    //表单操作弹窗
    gridHandle($event) {
        let arr=[];
        for(let obj of $event.columnApi._columnController.primaryColumns){
            if(obj['colDef']['count_table_id']){
                arr.push(obj);
            }
        }
        this.col_id=$event.data._id;
        this.colDef=arr;
        let THIS = this;
        let className = $event["event"]["target"]["className"];
        let id = $event["event"]["target"]["id"];
        this.action = $event["data"]["action"] || "";
        this.realId = $event["data"]["_id"];
        this.rowStatus = $event["data"]["status"];
        switch(className){
            case 'gridView':
                if( this.namespace == 'external' ) {
                    this.openAlert("该表为外部数据表，不可查看。",'success');
                    return;
                }
                if( this.permission.view == 0 ){
                    this.openAlert("没有查看权限。",'success');
                    return;
                }
                if(this.cannotopenform=='1'){
                    this.openAlert("已完成工作的子表不可查看",'success');
                    return;
                }
                if(this.cannotopenform=='2'){
                    this.openAlert("在途工作的表单不可查看",'success');
                    return;
                }
                this.setDisplay(1,true);
                break;
            case 'gridEdit':
                if( this.namespace == 'external' ) {
                    this.openAlert("该表为外部数据表，不可编辑。",'success');
                    return;
                }
                if( this.permission.edit == 0 ){
                    this.openAlert("没有编辑权限。",'success');
                    return;
                }
                if(this.cannotopenform=='1'){
                    this.openAlert("已完成工作的子表不可编辑",'success');
                    return;
                }
                if(this.cannotopenform=='2'){
                    this.openAlert("在途工作的表单不可编辑",'success');

                    return;
                }
                this.setDisplay(0,true,'tijiao');
                break;
            case 'gridHistory':
                this.isShowGridHistory = true;
                break;
            case 'customOperate':
                if(this.customOperateList){
                    for (let d of this.customOperateList) {
                        if (d["id"] == id) {
                            this.customOperate(d);
                        }
                    }
                }
                break;
            case 'rowOperation':
                for(let ro of this.rowOperation){
                    if(ro['row_op_id'] == id){
                        //在这里处理脚本
                        //如果前端地址不为空，处理前端页面
                        this.doRowOperation(ro,$event);
                    }
                }
                break;
        }
    }

    openBI: boolean = false;
    BIURL: SafeResourceUrl = '';

    //行级操作_前端操作
    rowOperationFrontend(data){
        this.lifeCycleData=data;
        this.rowDisplay = true;
        this.customRowId = data['rowId'];
        this.customTableId= data['table_id'];
        let row_op_id=data['row_op_id'];
        /*
         配置的前端地址格式——操作名（fun）:后台地址
         eg——execute:data/customize_router/?plugin_name=ServerPackageInfo.py
         */
        let fun=data['frontendAddress'].split(':')[0];
        let params=data['frontendAddress'].split(':')[1];
        //BI是否在新窗口打开
        this.openBI = false;
        this.BIURL = '';
        switch(fun){
            //行级操作-执行
            case 'sexecute':{
                //部署列表
                this.setDeloylist('f16',params,'execute');
                break;
            }
            case 'cexecute':{
                //部署列表
                this.setDeloylist('f23',params,'execute');
                break;
            }
            case 'export_row':{
                this.rowDisplay = false;
                return;
            }
            //行级操作-趋势
            case 'trend':{
                this.setDeloylist('f5',params,'trend');
                break;
            }
            //行级操作-趋势2
            case 'trend2':{
                this.setDeloylist('f5',params,'trend');
                break;
            }
            //行级操作-BI
            case 'bi':{
                params=[this.customTableId,this.realId,row_op_id,this.rowData,this.dgcService.cols];
                setTimeout(()=>{
                    this.dataRowOperateComponent.biFun(params);
                    this.openBI = true;
                    this.BIURL = this.dataRowOperateComponent.biIframeURL;
                },100);
                break;
            }
            case 'panorama':{
                this.panorama=false;
                let mywf=[
                    "一次立项流程",
                    "二次立项流程",
                    "项目内核流程",
                    "项目申报流程",
                    "发行管理流程"
                ];
                let overId;
                this.panoramaDataList=[];
                this.panoramaDataList2=[];
                let username="preview";
                if(UserService.userInfo && UserService.userInfo["username"]){
                    username = UserService.userInfo["username"];
                }
                let table_id = data.table_id;
                let form_id ="";
                let is_extra ="";
                let urlStatic = '/data/get_form_dynamic_data/?seqid=' + username + "&table_id=" + table_id + "&is_extra=" + is_extra + "&form_id=" + form_id;
                let json={
                    table_id:table_id,
                    is_view:0,
                    form_id:'',
                    parent_table_id:'',
                    parent_real_id:'',
                    parent_temp_id:'',
                    real_id:data.rowId
                };
                let tableUrl='/data/get_table_data/?table_id=' + table_id;
                let json2={
                    table_id:table_id,
                    first:0,
                    rows:100000,
                    parent_table_id:'',
                    parent_real_id:'',
                    parent_temp_id:''
                }
                this.wfService.getMydata(tableUrl,json).then(res=>{
                    let myRows=res.rows;
                    myRows.map((item,index)=>{
                        if(data.rowId==item._id){
                            this.searchKey=item.f5;
                            this.cd.markForCheck();
                        }
                    })
                });
                this.dataTableService.getRowData(urlStatic,json).then(res=>{
                    let json3={
                        table_id:table_id,
                        real_id:data.rowId,
                        count_id_list:[res.f8.id,res.f9.id,res.f10.id,res.f11.id,res.f12.id]
                    }
                    this.wfService.getFlowId(json3)
                        .then(
                            res=>{
                                overId=res;
                            }
                        );
                    //已完成
                    this.wfService.getWorkList({type:1,rows:100000,page:1})
                        .then((res)=>{
                            this.myDataAll=res.rows;
                            for(let i=0;i<this.myDataAll.length;i++){
                                if(this.myDataAll[i]['keyword']==this.searchKey){
                                    if(mywf.indexOf(this.myDataAll[i]['flow_name'])!=-1){
                                        this.panoramaDataList.push(this.myDataAll[i]);
                                    }
                                    if(mywf.indexOf(this.myDataAll[i]['flow_name'])==0){
                                        this.p1status.status=true;
                                    }
                                    if(mywf.indexOf(this.myDataAll[i]['flow_name'])==1){
                                        this.p2status.status=true;
                                    }
                                    if(mywf.indexOf(this.myDataAll[i]['flow_name'])==2){
                                        this.p3status.status=true;
                                    }
                                    if(mywf.indexOf(this.myDataAll[i]['flow_name'])==3){
                                        this.p4status.status=true;
                                    }
                                    if(mywf.indexOf(this.myDataAll[i]['flow_name'])==4){
                                        this.p5status.status=true;
                                    }
                                }
                            }
                            this.cd.markForCheck();
                            //进展中
                            this.wfService.getWorkList({type:0,rows:100000,page:1})
                                .then(res=>{
                                    this.myDataAll=res.rows;
                                    for(let i=0;i<this.myDataAll.length;i++){
                                        if(this.myDataAll[i]['keyword']==this.searchKey){
                                            if(mywf.indexOf(this.myDataAll[i]['flow_name'])!=-1){
                                                this.panoramaDataList2.push(this.myDataAll[i]);
                                                this.panoramaDataList.push(this.myDataAll[i]);
                                            }
                                            if(mywf.indexOf(this.myDataAll[i]['flow_name'])!=-1){
                                                this.panoramaDataList.push(this.myDataAll[i]);
                                            }
                                            if(mywf.indexOf(this.myDataAll[i]['flow_name'])==0){
                                                this.p1status.isStart=true;
                                            }
                                            if(mywf.indexOf(this.myDataAll[i]['flow_name'])==1){
                                                this.p2status.isStart=true;
                                            }
                                            if(mywf.indexOf(this.myDataAll[i]['flow_name'])==2){
                                                this.p3status.isStart=true;
                                            }
                                            if(mywf.indexOf(this.myDataAll[i]['flow_name'])==3){
                                                this.p4status.isStart=true;
                                            }
                                            if(mywf.indexOf(this.myDataAll[i]['flow_name'])==4){
                                                this.p5status.isStart=true;
                                            }
                                        }
                                    }
                                    this.panorama=true;
                                    this.cd.markForCheck();
                                });
                        });
                    if(res.f13.value){
                        this.specialstatus.value=res.f13.value;
                        this.cd.markForCheck();
                    }
                    if(res.f8.value){
                        this.p1status.value=res.f8.value;
                        this.cd.markForCheck();
                    }
                    if(res.f9.value){
                        this.p2status.value=res.f9.value;
                        this.cd.markForCheck();
                    }
                    if(res.f10.value){
                        this.p3status.value=res.f10.value;
                        this.cd.markForCheck();
                    }
                    if(res.f11.value.value){
                        this.p4status=res.f11.value;
                        this.cd.markForCheck();
                    }
                    if(res.f12.value.value){
                        this.p5status=res.f12.value;
                        this.cd.markForCheck();
                    }
                });
            }
            case 'lifecycle':{
                this.lifeCycleData=data;
                this.rowDisplay = false;
                this.lifecycle=true;
            }
        }
    }

    //把所需信息作为带参数，调用子组件方法
    setDeloylist(field:string,params:string,fun:string){
        setTimeout(()=>{
            this.dataRowOperateComponent[fun](params,this.dgcService.getDeployList(this.rowData,this.realId,field));
        },100);
    }

    //行级操作_后端操作
    rowOperationBackend(data,address){
        let This=this;
        $.ajax({
            method:'POST',
            url:address,
            data:data,
            success:function (res) {
                if(res){
                    if(res['success']==1){
                        This.openAlert('已经向服务器发送请求', 'confirm')
                    }else if(res['success']==0){
                        This.openAlert('发送请求失败！错误是'+res['error'], 'error');
                    }
                }
            }
        })
    }

    //自定义操作窗口弹射上来的值（提交成功后的回调）
    customSuccess(result) {
        if (result) {
            this.customDisplay = false;
        }
    }
    //行级操作窗口弹射上来的值（提交成功后的回调）
    rowSuccess(result) {
        if (result) {
            this.rowDisplay = false;
        }
    }

    //前面的自定义操作
    customOperate(d) {
        if (this.realId == 0) {
            return;
        }
        this.customFlowId = d["flow_id"];
        this.customFormId = d["form_id"];
        this.customId = d["id"];
        this.customTableId = d['table_id'];
        this.customRowId = this.realId;
        this.customDisplay = true;
    }

    //表单row双击事件
    onRowDoubleClicked($event) {
        if(this.isEditable){
            return;
        }
        if( this.cannotDoubleClick ){
            return;
        }
        if (!$event.data || ( this.namespace == 'external' ) || this.permission['view']==0) {
            return;
        }
        if( $event.data.group ){
            return;
        }
        if ((new RegExp(require('./icon_handle.png'))).test($event.event.target.style.backgroundImage)) {
            return;
        }
        if(this.cannotopenform=='1'){
            this.openAlert("已完成工作的子表不可查看",'success');
            return;
        }
        if(this.cannotopenform=='2'){
            this.openAlert("在途工作的表单不可查看",'success');
            return;
        }
        if (this.datagridStatus) { //查看模式下
            this.action = $event["data"]["action"] || "";
            this.realId = $event["data"]["_id"];
            //个人用户编辑表中的数据时,会发起对应的工作流,不让用户编辑
            this.setDisplay(1,true,"",!($event["data"]["status"] && $event["data"]["status"] == 2));
        }
    }

    //单元格修改返回值
    emitFormData(data) {
        let changeJson = {
            newValue: data[this.editField_f],
            oldValue: this.rowData[this.editRowIndex][this.editField_f],
            colDef: this.currentValue
        };
        this.dgcService.onCellValueChanged(changeJson,this.changeFieldList,this.newValueChangeList,this.oldValueChangeList);
        this.rowData[this.editRowIndex][this.editField_f] = data[this.editField_f]
        this.gridOptions_top.api.setRowData(this.rowData)
        this.datagridEditWin = false;
    }

    onCellDoubleClicked($event) {
        if(this.isEditable){
            return;
        }
        if (!this.datagridStatus) {
            let colDef = $event.colDef,
                tableData = $event.data;
            //子表和统计 ，checkbox 和 查看编辑，默认无法修改
            if (this.dgcService.childTable(colDef['dinput_type']) || this.dgcService.countTable(colDef['dinput_type']) || colDef['colId'] == 0 ) {
                return;
            }
            this.currentValue = colDef;
            this.rowId = tableData._id;
            this.fieldId = colDef['id'];
            this.datagridEditWin = !this.datagridEditWin;
            this.editField_f = colDef['field'];
            this.editRowIndex = $event.rowIndex;
        }
    }

    onRowDataChanged ($event){
        if( this.rowData ){
            this.globalService.sonGridTotal.next( {total:this.rowData.length,tableId:this.pageId} );
        }
    }

    //编辑成功后
    isSuccess(data) {
        // if( this.parentTableId ){
        //     this.dataTableService.refreshDataTableId.push( this.parentTableId );
        // }else{
        //     this.dataTableService.refreshParentDataTableId.push( this.pageId );
        // }
        if (data) {
            this.display = false;
            this.getBodyData(true,this.correspondenceSelectedList);
            // this.openAlert('提交成功','success');
            //请求提醒数据
            this.remindService.getRemindInfo(this.pageId).then(res => {
                this.dgcService.setRemindColor(res);
                // 解决编辑后隐藏列改变的问题
                // this.getOrSetGridState( true,false );
                // this.dgcService.getHiddenColumnIds();
                // this.columnDefs = this.dgcService.createHeaderColumnDefs(this.emitReset,
                //     this.oHandle,this.isFixed,this.permission['edit'],this.customOperateList,this.rowOperation,
                //     this.fieldContent,this.tableType, this.source_field_dfield,this.viewMode,false );
                // this.dgcService.calcColumnState();
                // this.getOrSetGridState(false,true);
            })
        }
    }

    //创建搜索
    createFilter({data, sHtml, fn, searchOperate}) {
        let col_field = data.column.colDef.field,
            This = this,
            oDiv = document.createElement('div'),
            oSelect = document.createElement('select'),
            oInput = document.createElement('input');

        let searchType = this.dgcService.createSelectAndInputForFilter(oSelect,oInput,sHtml,data);
        //请不要写成链式写法，会出不来搜索框
        // oDiv.appendChild(oSelect);
        oDiv.appendChild(oInput);

        oInput.addEventListener( searchType, function ($event) {
            if( $event['keyCode'] != 229 ){
                This.keyupSearch($event,oInput,This,col_field,data,searchType,searchOperate);
            }
        });
        if( searchType == 'keyup' ){
            oInput.addEventListener( 'keydown', function ($event) {
                if( $event.keyCode == 229 ){
                    This.keyupSearch($event,oInput,This,col_field,data,searchType,searchOperate);
                }
            });
        }
        return oDiv;
    }

    keyupSearch($event,oInput,This,col_field,data,searchType,searchOperate){
        setTimeout( ()=>{
            //解决汉字搜素第一次传空' '的问题
            let keyArr = [32,37,38,39,40]
            if( $event['keyCode']&&keyArr.indexOf($event['keyCode'])==-1 ){
                if( oInput.value[oInput.value.length-1] == ' ' || oInput.value == "" && !This.dgcService.searchOldValue[col_field] ){
                    return;
                }
                for( let i=0;i<oInput.value.length;i++ ){
                    if( oInput.value[i]==' ' && (( This.dgcService.searchOldValue[col_field] && This.dgcService.searchOldValue[col_field][i] && This.dgcService.searchOldValue[col_field][i] != ' ' )) ){
                        return;
                    }
                }
            }
            let keyWord = data.column.colDef.isNum ? Number(oInput.value) : oInput.value;
            if( oInput.value == "" ){
                keyWord = oInput.value;
            }
            if( This.dgcService.searchOldValue[col_field] && This.dgcService.searchOldValue[col_field] == keyWord ){
                return;
            }else {
                This.dgcService.searchOldValue[col_field] = keyWord;
            }
            //日期时间情况下
            if( searchType == 'change' ){
                if( keyWord.indexOf( 'T' ) != -1 ){
                    keyWord = keyWord.replace( 'T',' ' );
                }
            }
            This.dgcService.searchValue[col_field] = oInput.value;
            let obj_2 = This.dgcService.filterPostData(col_field,keyWord,searchOperate,This.postData);
            This.dgcService.searchDataState = obj_2['filter'] || null;
            This.justSearch( obj_2 );
        },0 )
    }

    //去掉字符串后边空格
    RTrim(str){
        let i;
        for(i=str.length-1;i>=0;i--){
            if(str.charAt(i)!=" "){
                break;
            }
        }
        str = str.substring(0,i+1);
        return str;
    }

    justSearch ( obj_2 ){
        obj_2 = this.dgcService.getSearchAndSort( obj_2,this.tableType,this.keyword,this.isShowGroup,this.myGroup );
        this.dataTableService.getDataTableList( obj_2 ).then(res=> {
            if( this.viewMode == 'editFromCorrespondence' ){
                this.setCorrespondenceLists(res,false);
            }
            this.total = res.total;
            this.rowData=res.rows;
            let myJson = {
                table_id : this.pageId,
                specical_count_field_infos : this.flow_node,
                rows : this.rowData
            };
            if ( this.flow_node.length > 0 ){
                this.dfService.getFlowNodeInfo(myJson)
                    .then(
                        res=>{
                            for( let i = 0 ; i < this.flow_node.length ; i++)
                            {
                                for( let j = 0 ; j < res.rows.length ; j++ )
                                {
                                    if( !res.define_infos[res.rows[j]._id] ){
                                        res.rows[ j ][ this.flow_node[i].dfield ] = res.rows[ j ][ this.flow_node[i].dfield ] == '' ? '' : '结束';
                                    }
                                    else if( !res.define_infos[ res.rows[ j ]._id ][ this.flow_node[i].dfield ] )
                                    {
                                        res.rows[ j ][ this.flow_node[i].dfield ] = res.rows[ j ][ this.flow_node[i].dfield ] == '' ? '' : '结束';
                                    }
                                }
                            }
                            this.define_real = res.rows;
                            this.define_infos = res.define_infos;
                            this.rowData = res.rows;
                            try{
                                if(this.gridOptions_top.api) {
                                    this.gridOptions_top.api.setRowData(this.rowData);
                                    if(!this.autoResizeWidth){
                                        this.gridOptions_top.columnApi.autoSizeColumns(this.dgcService.getAllColumnIds(this.columnDefs));
                                    }
                                    this.gridOptions_top.api.refreshView();
                                }
                            }catch (err){}
            });} else {
                    this.gridOptions_top.api.setRowData(res.rows);
                }
            if( this.viewMode == 'editFromCorrespondence' ){
                this.showAllCorrespondenceData( res.rows )
            }
            //修改搜索后分页条数显示问题
            this.dataGridMissionService.missionTotal( res.total );
            this.sortWay();
            this.gridOptions_top.api.refreshView();
            this.cd.markForCheck();
        });
        this.searchFooter( obj_2 );
    }

    //搜索同时也搜索footer的数据
    searchFooter ( postData ){
        //搜索同时也搜索footer的数据
        this.dataTableService.getFootDataWithoutCache(postData)
            .then(res => {
                let obj = res.rows[0] || {};
                obj["myfooter"] = '合计';
                let arr=[obj];
                this.rowData_footer = arr;
                //处理footer提示undefine
                this.dgcService.clearFooterUndefined(this.columnDefs_bottom,this.rowData_footer);
                this.cd.markForCheck();
                this.setEditClickable();
                //this.openAlert("刷新成功", 'success');
                this.isRefreshFinish.emit(true);
            })
    }

    // 前端搜索
    startFilter(searchOperate, keyWord, col_field) {
        var filterApi = this.gridOptions_top.api.getFilterApi(col_field);
        let model = {type:filterApi[searchOperate],filter:keyWord};
        filterApi.setModel(model);
        this.gridOptions_top.api.onFilterChanged();
    }

    // 取消前端搜索
    destroyFilter(col_field) {
        this.gridOptions_top.api.destroyFilter(col_field);
    }

    ngOnDestroy() {
        this.missionQuerySourceSub.unsubscribe();
        this.idsImportedSub.unsubscribe();
        this.addOptionSub.unsubscribe();
        if(this.userInfoSub){
            this.userInfoSub.unsubscribe();
        }
        this.dataSearchSub.unsubscribe();
        //重置状态下不保存
        if(this.goSearchConSub){
            this.goSearchConSub.unsubscribe();
        }
        if(this.dataInvaliSub){
            this.dataInvaliSub.unsubscribe();
        }
        if( !this.dgcService.isReset ){
            this.saveColWidth();
            //关闭时保存拖动的数据
            if( this.isSaveDrag ){
                this.dataTableService.savePreference({
                    'action': 'fieldsOrder',
                    table_id: this.pageId,
                    fieldsOrder: JSON.stringify( this.dragArr )
                });
            }
        }
        // this.documentClickListener();
    }

    setColumnState(state){
        if(this.gridOptions_top.columnApi){
            try{this.gridOptions_top.columnApi.setColumnState( state );}catch(e){}
        }
    }

    groupUpdate(){
        let postData = this.postData;
        postData['tableType'] = 'group';
        postData['is_group'] = 1;
        postData['group_fields'] = this.myGroup;
        this.justSearch( postData );
    }
    isEditable : boolean =false;
    isEditClickable: boolean = true;

    onEditCancel(){
        this.isEditable = false;
        this.dgcService.setEditIcon('editable');
        this.gridOptions_top.api.stopEditing(false);
        this.gridOptions_top.api.setColumnDefs(this.columnDefs);
        this.justSearch(this.postData);
        this.setGridSort();
        this.getOrSetGridState( false,false );
    }

    idCache = {};
    changedRowNum = 0;
    countEditNum: number = 0;

    //----------------------------Yunxuan Yan-------------------------
    //This is used to set datagrid editable
    onEdit(cancel?:boolean){
        this.isEditable = !this.isEditable;
        //保存编辑数据
        this.getOrSetGridState( true,true );
        if(!this.isEditable){
            this.gridOptions_top.api.stopEditing(false);
            this.gridOptions_top.api.setColumnDefs(this.columnDefs);
            let state = this.dgcService.calcColumnState();
            // this.setColumnState(state);
            //比对当前值与初始值的差别
            let changedRows = this.dgcService.getChangedRows(this.rowData);
            this.changedRowNum = Object.getOwnPropertyNames(changedRows).length;
            this.countEditNum = 0;
            if(cancel){
                return this.changedRowNum !=0?true:false;
            }
            this.isEditClickable = false;
            this.dgcService.setEditIcon('unEditable');
            if(!this.alertVisible){
                this.openAlert('数据保存中','success');
            }
            //保存编辑成功后存入失效刷新的tableID
            if( this.parentTableId ){
                this.globalService.refreshDataTableId.push( this.parentTableId );
            }else{
                this.globalService.refreshParentDataTableId.push( this.pageId );
            }
            this.alertVisible = true;
            if(this.changedRowNum != 0){
                for(let k in changedRows){
                    let changed = changedRows[k];
                    let real_id = changed['data']['real_id'];
                    changedRows[real_id] = changed;
                    // if(!this.idCache[real_id]){
                        this.dfService.getDynamicFormData({
                            table_id: this.pageId,
                            real_id: real_id,
                            is_view: 0
                        })
                    // } else {
                    //     this.saveEdit(changed,this.idCache[real_id]);
                    // }
                }
            } else {
                this.dgcService.setEditIcon('editable');
                this.isEditClickable = true;
                this.setGridSort();
                this.getOrSetGridState( false,false );
                return;
            }
            //保存
            let subscribe = this.dfService.dynamicFormDataSub.subscribe(res=>{
                if(res){
                    let data = res['data'];
                    let real_id = data['real_id']['value'];
                    if(!changedRows[real_id]){
                        return;
                    }
                    let obj = {
                        real_id:data['real_id']['value'],
                        temp_id:data['temp_id']['value'],
                        parent_real_id:data['parent_real_id']['value'],
                        parent_table_id:data['parent_table_id']['value'],
                        parent_temp_id:data['parent_temp_id']['value']
                    };
                    let targetRow = changedRows[real_id];
                    this.idCache[real_id] = obj;
                    this.saveEdit(targetRow,obj);
                    this.countEditNum++;
                    if( this.countEditNum == this.changedRowNum ){
                        subscribe.unsubscribe();
                    }
                }
            });
        } else {
            this.dgcService.setEditIcon('save');
            this.isEditClickable = true;
            this.gridOptions_top.api.setColumnDefs(this.dgcService.columnEditDefs);
            this.getOrSetGridState( false,false );
            this.setGridSort();
        }
    }
    //End----------------------------Yunxuan Yan-------------------------

    setGridSort (){
        if( this.dgcService.sortDataState ){
            this.gridOptions_top.api.setSortModel( [{colId: this.dgcService.sortDataState.sortField, sort: this.dgcService.sortDataState.sortOrder == -1?'desc':'asc'}] )
        }
    }

    saveEdit(targetRow,ids){
        let json=this.dgcService.abjustTargetRow(targetRow,ids);
        json['data']=encodeURIComponent(JSON.stringify(json['data']));
        json['cache_new']=encodeURIComponent(JSON.stringify(json['cache_new']));
        json['cache_old']=encodeURIComponent(JSON.stringify(json['cache_old']));
        this.wfService.saveAddpageData(json).then(res=>{
            if(this.imgType == 'success'){
                this.alertVisible = false;
            }
            if(res['success']==1){
                console.log('保存'+ids['real_id']+'的数据成功！');
            } else {
                this.openAlert(res['error'],'error');
                //保存编辑失败后删除失效刷新的tableID
                this.globalService.refreshDataTableId = [];
                this.globalService.refreshParentDataTableId = [];
            }
            this.changedRowNum--;
            if(this.changedRowNum <= 0){
                this.cd.markForCheck();
            }
            this.setEditClickable();
            this.setGridSort();
            this.getOrSetGridState( false,false );
        });
    }

    setEditClickable(){
        this.isEditClickable = true;
        this.dgcService.setEditIcon('editable');
    }
    visibleChange($event){
        this.panorama=false;
        this.searchKey="";
        this.p1status={};
        this.p2status={};
        this.specialstatus={};
        this.p3status={};
        this.p4status={};
        this.p5status={};
    }

    @ViewChild(CustomColumnsComponent)
    CustomColumnsComponent: CustomColumnsComponent;

    //data-grid宽
    gridWidth: any = '100%';

    //定制列开关
    isShowCustomColumns: boolean = false;

    //列数据
    columns: any = [];

    fixCols: any = {
        r:[],
        l:[]
    }

    //定制列的显示以及隐藏
    customColumns (){
        this.isShowCustomColumns = !this.isShowCustomColumns;
        this.returnWidth();
        this.cd.markForCheck();
    }

    //是否显示分组面板
    isShowGroupPanel: boolean = false;

    groupPanel (){
        if( !this.isShowGroupPanel ){
            this.gridOptions_top['getNodeChildDetails'] = this.getNodeChildDetails;
        }else {
            delete this.gridOptions_top['getNodeChildDetails'];
        }
        setTimeout( ()=>{
            this.isShowGroupPanel = !this.isShowGroupPanel;
            if( !this.isShowGroupPanel ){
                this.showData();
            }
            this.isShowGroup = this.isShowGroupPanel;
            this.gridOptions_top.columnApi.setColumnVisible( 'group' , this.isShowGroup)
            this.returnWidth();
            this.cd.markForCheck();
        },100 )
    }

    //返回data-grid宽度
    returnWidth (){
        let width = '100%';
        if( this.isShowCustomColumns || this.isShowGroupPanel ){
            width = 'calc( 100% - 200px)';
        }
        if( this.isShowCustomColumns && this.isShowGroupPanel ){
            width = 'calc( 100% - 400px)';
        }
        this.gridWidth = width;
        this.cd.markForCheck();
    }

    onAfterHide ($event){
        this.display = false;
        this.cd.markForCheck();
    }

    ids:any={};
    values:any=[];
    tabIndex:number=0;
    //合同模板预览
    showContract(data){
        this.isShowContract=true;
        this.isShowContractLoading=true;
        //let ids={table_id:this.pageId,real_id:data.data._id,field_id:data.colDef.id};
        let model_id=this.values[this.tabIndex].model_id;
        let ele=this.values[this.tabIndex].elements;
        this.ceService.getContract(this.ids,model_id,ele,"show",this.tabIndex).then(res=>{
            if(res.success==1){
                if(res.data.content){
                    this.showContractHTML=res.data.content;
                }else{
                    this.showContractHTML=res.error;
                }
            }else{
                this.showContractHTML="服务器返回了一个错误."
            }
            this.isShowContractLoading=false;
            this.cd.markForCheck();
        })
    }

    downloadIndex:number=0;
    //合同模板下载
    isDownloadContract(data){
        if(this.downloadIndex==this.values.length){
            this.downloadIndex=0;
            return;
        }
        let model_id = this.values[this.downloadIndex].model_id;
        let k2v = this.values[this.downloadIndex].k2v;
        let fileName=this.values[this.downloadIndex].name;
        for (let key in k2v) {
            k2v[key] = k2v[key].replace(/&nbsp;/g, " ").replace(/<br>/g, "");
        }
        this.ceService.downloadContract(this.ids, model_id, k2v, fileName).then(res=>{
            if(res.success==1){
                this.downloadIndex++;
                this.isDownloadContract(data);
            }
        })
    }

    //切换标签
    toggleTab($event,value){
        console.log(value);
        this.isShowContractLoading=true;
        this.tabIndex=$event.target.id;
        this.showContract($event);
    }
}